import java.util.*
import kotlin.math.pow


fun rotateString(s: String, goal: String): Boolean {
    var isEqual = false
    var temp = s
    for(i in 0..temp.length){
        if( temp == goal ){
            isEqual = true
            break
        }
        temp += temp[0]
        temp = temp.removeRange(0,1)
    }
    return isEqual
}

fun runningSum(nums: IntArray): IntArray {
    val sumArray = IntArray(nums.size)
    sumArray[0] = nums[0]
    for ( i in 1 until nums.size){
        sumArray[i] = sumArray[i-1] + nums[i]
    }
    return  sumArray
}

fun groupAnagrams(strs: Array<String>): List<List<String>> {
    val groupHash = mutableMapOf<String, MutableList<String>>()
    for( str in strs ){
        println(str)
        val sortStr = String(str.toCharArray().sortedArray())
        if ( groupHash.containsKey(sortStr) ){
            groupHash[sortStr]!!.add(str)
        }else{
            groupHash[sortStr] = mutableListOf(str)
        }
    }
    return groupHash.map {
        it.value
    }
}

fun numIdenticalPairs(nums: IntArray): Int {
    val numHash = mutableMapOf<Int, Int>()
    var count = 0
    for(num in nums){
        if( numHash.containsKey(num) ){
            count += numHash[num]!! + 1
            numHash[num] = numHash[num]!!.plus(1)
        }else{
            numHash[num] = 0
        }
    }
    return count
}

fun numberOfMatches(n: Int): Int {
    var remainTeam = n
    var count = 0

    while ( remainTeam > 1 ){
        count += remainTeam / 2
        remainTeam = remainTeam / 2 + remainTeam % 2
        println(count)
        println(remainTeam)
    }

    return count
}

fun findRestaurant(list1: Array<String>, list2: Array<String>): Array<String> {
    val listHash1 = mutableMapOf<String, Int>()
    for(i in list1.indices){
        listHash1[list1[i]] = i
    }
    var commArr = mutableListOf<String>()
    var commCount = Integer.MAX_VALUE
    for(_index2 in list2.indices){
        listHash1[list2[_index2]]?.let { _index1 ->
            if( _index1 + _index2 == commCount ){
                commArr.add(list2[_index2])
            }
            if ( _index1 + _index2 < commCount){
                commCount = _index1 + _index2
                commArr = mutableListOf(list2[_index2])
            }
        }
    }
    return commArr.toTypedArray()
}

fun minStartValue(nums: IntArray): Int {
    var min = Integer.MAX_VALUE
    var count = 0
    for( num in nums ){
        count += num
        if( count < min ){
            min = count
        }
    }

    min = if( min < 0 ){
        1 - min
    }else{
        1
    }

    return min
}

fun findKthLargest(nums: IntArray, k: Int): Int {
    return nums.sorted()[nums.size-k]
}

class NumArray(nums: IntArray) {
    val nums = nums
    fun sumRange(left: Int, right: Int): Int {
        var count = 0
        for( i in left..right){
            count += nums[i]
        }
        return count
    }
}

//20. Valid Parentheses
fun isValid(s: String): Boolean {
    var valid = true
    var brackStack = mutableListOf<Char>()
    for ( i in s ){
        if ( i == '(' || i == '[' || i == '{'){
            brackStack.add(i)
        }else if ( i == ')' && brackStack.isNotEmpty() && brackStack.last() == '('){
            brackStack.removeAt(brackStack.lastIndex)
        }else if ( i == ']' && brackStack.isNotEmpty() && brackStack.last() == '['){
            brackStack.removeAt(brackStack.lastIndex)
        }else if ( i == '}' && brackStack.isNotEmpty() && brackStack.last() == '{'){
            brackStack.removeAt(brackStack.lastIndex)
        }else{
            valid = false
            break
        }
    }
    return valid && brackStack.isEmpty()
}

fun sumDigit(num: Int): Int{
    var sum = 0
    var curNum = num
    while( curNum != 0 ){
        sum += curNum % 10
        curNum /= 10
    }
    return sum
}

fun countBalls(lowLimit: Int, highLimit: Int): Int {
    var curMaxCount = 0
    val boxHash = mutableMapOf<Int, Int>()
    for ( i in lowLimit..highLimit){
        val sum = sumDigit(i)
        boxHash[sum] = boxHash[sum]?.plus(1) ?: 1
        if( boxHash[sum]!! > curMaxCount ){
            curMaxCount = boxHash[sum]!!
        }
    }

    return curMaxCount
}

fun findLucky(arr: IntArray): Int {
    val courtArray = IntArray(501){0}
    var luckyVal = -1
    for( num in arr ){
        courtArray[num] += 1
    }
    for ( i in courtArray.size-1 downTo 1 ){
        if( courtArray[i] == i ){
            luckyVal = i
            break
        }
    }
    return luckyVal
}

fun areOccurrencesEqual(s: String): Boolean {
    val countHash = mutableMapOf<Char, Int>()
    for( c in s){
        countHash[c] = countHash[c]?.plus(1) ?: 1
    }
    val countVal = countHash.entries.first().value
    var isEqual = true
    for ( (key, value ) in countHash ){
        if( value != countVal ){
            isEqual = false
            break
        }
    }
    return isEqual
}

fun sumOfUnique(nums: IntArray): Int {
    val countHash = mutableMapOf<Int, Int>()
    var sum = 0
    for( num in nums ){
        countHash[num] = countHash[num]?.plus(1) ?: 1
        val curCount = countHash[num]
        if( curCount == 1 ){
            sum += num
        }else if ( curCount == 2){
            sum -= num
        }
    }
    return sum
}

fun checkIfExist(arr: IntArray): Boolean {
    val numHash = mutableMapOf<Int,Int>()
    var isExist = false
    for ( num in arr ){
        if( numHash.containsKey(num*2) ){
            isExist = true
            break
        }else if( num % 2 == 0 && numHash.containsKey(num/2) ){
            isExist = true
            break
        }
        numHash[num] = 0
    }
    return isExist
}

fun heightChecker(heights: IntArray): Int {
    val expected = heights.sorted()
    var count = 0
    for ( i in heights.indices){
        if ( expected[i] != heights[i] ){
            count++
        }
    }
    return count
}

fun numOfStrings(patterns: Array<String>, word: String): Int {
    var count = 0
    for ( pattern in patterns ){
        if( word.contains(pattern) ){
            count++
        }
    }
    return count
}

fun arrayRankTransform(arr: IntArray): IntArray {
    val sortedArray = arr.sorted()
    var pos = 1
    val posHash = mutableMapOf<Int,Int>()
    for ( num in sortedArray ){
        if ( !posHash.containsKey(num) ){
            posHash[num] = pos++
        }
    }
    val rankArray = IntArray(arr.size)
    for ( i in rankArray.indices ){
        rankArray[i] = posHash[arr[i]]!!
    }
    return rankArray
}

fun missingNumber(nums: IntArray): Int {
    val expectedSum = nums.size * (nums.size + 1) / 2
    var actualSum = 0
    for( num in nums ){
        actualSum += num
    }
    return expectedSum - actualSum
}

fun squareIsWhite(coordinates: String): Boolean {
    val x = coordinates[0] - 'a' + 1
    val y = coordinates[1] - '1' + 1
    val sum = x + y
    var isWhite = false
    if( sum % 2 != 0 ){
        isWhite = true
    }
    return isWhite
}

fun majorityElement(nums: IntArray): Int {
    val majThreshold = nums.size / 2
    var majVal = 0
    val numCount = mutableMapOf<Int,Int>()
    for ( num in nums ){
        numCount[num] = numCount[num]?.plus(1) ?: 1
        if( numCount[num]!! > majThreshold ){
            majVal = num
        }
    }
    return majVal
}

//1952. Three Divisors
fun isThree(n: Int): Boolean {
    var count = 0
    for( i in 1..n ){
        println(count)
        if( n % i == 0){
            count++
        }
    }
    return count == 3
}

//1287. Element Appearing More Than 25% In Sorted Array
fun findSpecialInteger(arr: IntArray): Int {
    val threshold = arr.size / 4
    var count = 1
    var maj = arr[0]
    for( i in 1 until arr.size ){
        if( count > threshold ){
            break
        }
        if ( maj == arr[i]  ){
            count++
        }else{
            maj = arr[i]
            count = 1
        }
    }
    return maj
}

//344. Reverse String
fun reverseString(s: CharArray): Unit {
    for ( i in 0 until s.size/2){
        val temp = s[i]
        s[i] = s[s.size-i-1]
        s[s.size-i-1] = temp
    }
}

//704. Binary Search
fun search(nums: IntArray, target: Int): Int {
    var head = 0
    var tail = nums.size-1
    var index = -1
    var middle: Int
    var temp: Int
    while ( head <= tail){
        middle = ( head + tail ) / 2
        temp = nums[middle]
        if( temp == target ){ //find target
            index = middle
            break
        }else if ( temp < target ){ // target on right side
            head = middle + 1
        }
        else{ // target on left side
            tail = middle - 1
        }
    }
    return index
}

//347. Top K Frequent Elements
fun topKFrequent(nums: IntArray, k: Int): IntArray {
    val countHash = mutableMapOf<Int, Int>()
    for (num in nums){
        countHash[num] = countHash[num]?.plus(1) ?: 1
    }
    val sortedMap = countHash.entries.sortedBy { it.value }.reversed()
    val frequencyArr = IntArray(k)
    for ( i in 0 until k){
        frequencyArr[i] = sortedMap[i].key
    }
    return frequencyArr
}

//1437. Check If All 1's Are at Least Length K Places Away
fun kLengthApart(nums: IntArray, k: Int): Boolean {
    var preOnePos = -1
    var isKApart = true
    for ( i in nums.indices ){
        if ( nums[i] == 1 ){
            if ( i - preOnePos - 1 >= k || preOnePos == -1){
                preOnePos = i
            }else{
                isKApart = false
                break
            }
        }
    }
    return isKApart
}

//380. Insert Delete GetRandom O(1)
class RandomizedSet() {

    /** Initialize your data structure here. */
    private val mapTable = mutableMapOf<Int, Int>()
    private val valList = mutableListOf<Int>()
    private val random = Random()

    /** Inserts a value to the set. Returns true if the set did not already contain the specified element. */
    fun insert(`val`: Int): Boolean {
        if( mapTable.containsKey(`val`) ){
            return false
        }
        valList.add(`val`)
        mapTable[`val`] = valList.size-1
        return true
    }

    /** Removes a value from the set. Returns true if the set contained the specified element. */
    fun remove(`val`: Int): Boolean {
        if( mapTable.containsKey(`val`) ){
            val index = mapTable[`val`]
            val tempVal = valList.last()
            valList[index!!] = tempVal
            mapTable[tempVal] = index
            mapTable.remove(`val`)
            valList.removeAt(valList.size-1)

            return true
        }
        return false
    }

    /** Get a random element from the set. */
    fun getRandom(): Int {
        return valList.get(random.nextInt(valList.size))
    }

}

//492. Construct the Rectangle
fun constructRectangle(area: Int): IntArray {
    var length = Math.ceil(Math.sqrt(area.toDouble())).toInt()
    var width = 0
    while (true){
        if( area % length == 0 ){
            width = area / length
            break
        }else{
            length++
        }
    }
    return intArrayOf(length, width)
}

//1470. Shuffle the Array
fun shuffle(nums: IntArray, n: Int): IntArray {
    val output = IntArray(nums.size)
    var left = 0
    var right = n
    for ( i in output.indices ){
        if( i % 2 == 0 ){
            output[i] = nums[left]
            left++
        }else{
            output[i] = nums[right]
            right++
        }
    }
    return output
}

//1281. Subtract the Product and Sum of Digits of an Integer
fun subtractProductAndSum(n: Int): Int {
    var product = 1
    var sum = 0
    var num = n
    while( num > 0 ){
        val digit = num % 10
        product *= digit
        sum += digit
        num /= 10
    }
    return product - sum
}

//1603. Design Parking System
class ParkingSystem(big: Int, medium: Int, small: Int) {
    var bigSpace = big
    var mediumSpace = medium
    var smallSpace = small
    var bigCount = 0
    var mediumCount = 0
    var smallCount = 0


    fun addCar(carType: Int): Boolean {
        return when( carType ){
            1 -> {
                return if ( bigCount == bigSpace ){
                    false
                }else{
                    bigCount++
                    true
                }
            }
            2 -> {
                return if ( mediumCount == mediumSpace ){
                    false
                }else{
                    mediumCount++
                    true
                }
            }
            3 -> {
                return if ( smallCount == smallSpace ){
                    false
                }else{
                    smallCount++
                    true
                }
            }
            else -> false
        }
    }

}

//278. First Bad Version
fun isBadVersion(version: Int): Boolean{
    return false
}
fun firstBadVersion(n: Int) : Int {
    var left = 1
    var right = n
    while (left < right) {
        val mid = left + (right - left) / 2 //avoid overflow
        if (isBadVersion(mid)) {
            right = mid
        } else {
            left = mid + 1
        }
    }
    return left
}

//35. Search Insert Position
fun searchInsert(nums: IntArray, target: Int): Int {
    var head = 0
    var tail = nums.size - 1
    while( head <= tail ){
        val middle = head + ( tail - head ) / 2
        val temp = nums[middle]
        if( temp == target ){
            return middle
        }else if( temp < target ){
            head = middle + 1
        }else{
            tail = middle - 1
        }
    }
    return head
}

//977. Squares of a Sorted Array
//use two pointer
fun sortedSquares(nums: IntArray): IntArray {
    var left = 0
    var right = nums.size - 1
    val sortedSquareList = IntArray(nums.size)
    for ( i in sortedSquareList.indices ){
        val leftSquare = nums[left] * nums[left]
        val rightSquare = nums[right] * nums[right]
        if( leftSquare < rightSquare ){
            sortedSquareList[nums.size - 1 - i] = rightSquare
            right--
        }else{
            sortedSquareList[nums.size - 1 - i] = leftSquare
            left++
        }
    }
    return sortedSquareList
}

//189. Rotate Array
fun reverse(nums: IntArray, start: Int, end: Int){
    var left = start
    var right = end
    while ( left < right ){
        val temp = nums[left]
        nums[left] = nums[right]
        nums[right] = temp
        left++
        right--
    }
}

//cal real rotate num
fun rotate(nums: IntArray, l: Int): Unit {
    val k = l % nums.size //l may greater than nums.size
    reverse(nums, 0, nums.size-1) //reverse whole array
    reverse(nums, 0, k-1) //reverse first k
    reverse(nums, k, nums.size-1) //reverse remain
}

//283. Move Zeroes
fun moveZeroes(nums: IntArray): Unit {
    var toChangePos = 0
    //move non-zero foreword
    for ( i in nums.indices){
        if( nums[i] != 0 ){
            nums[toChangePos] = nums[i]
            toChangePos++
        }
    }
    //fill remaining zero
    for ( i in toChangePos until nums.size){
        nums[i] = 0
    }
}

//167. Two Sum II - Input array is sorted
fun twoSum(numbers: IntArray, target: Int): IntArray {
    var left = 0
    var right = numbers.size-1
    while ( left < right ){
        val sum = numbers[left] + numbers[right]
        if( sum == target ){
            break
        }else if( sum < target ){
            left++
        }else{
            right--
        }
    }
    return intArrayOf(left+1, right+1)
}

//1299. Replace Elements with Greatest Element on Right Side
fun replaceElements(arr: IntArray): IntArray {
    var max = arr.last()
    for ( i in arr.size-2 downTo 0 ){
        val curVal = arr[i]
        //replace
        arr[i] = max
        //get right side max
        if ( curVal > max ){
            max = curVal
        }
    }
    arr[arr.size-1] = -1
    return arr
}

//557. Reverse Words in a String III
fun reverseString(s: String): String{
    val outputBuilder = StringBuilder()
    for ( i in s.length-1 downTo 0){
        outputBuilder.append(s[i])
    }
    return outputBuilder.toString()
}
fun reverseWords(s: String): String {
    val words = s.split(" ")
    val output = StringBuilder()
    for ( word in words ){
        output.append(reverseString(word)).append(" ")
    }
    output.deleteCharAt(output.length-1)
    return output.toString()
}

//876. Middle of the Linked List
class ListNode(var `val`: Int) {
    var next: ListNode? = null
}
fun middleNode(head: ListNode?): ListNode? {
    var slowNode = head
    var fastNode = head
    while ( fastNode != null && fastNode.next != null){
        slowNode = slowNode!!.next
        fastNode = fastNode.next!!.next
    }
    return slowNode
}

//19. Remove Nth Node From End of List
fun removeNthFromEnd(head: ListNode?, n: Int): ListNode? {
    val start = ListNode(0)
    var slow: ListNode? = start
    var fast: ListNode? = start
    slow!!.next = head

    //Move fast in front so that the gap between slow and fast becomes n
    for (i in 1..n + 1) {
        fast = fast!!.next
    }
    //Move fast to the end, maintaining the gap
    while (fast != null) {
        slow = slow!!.next
        fast = fast.next
    }
    //Skip the desired node
    slow!!.next = slow.next!!.next
    return start.next
}

//3. Longest Substring Without Repeating Characters
fun lengthOfLongestSubstring(s: String): Int {
    val charHash = mutableMapOf<Char,Int>()
    var max = 0
    var i = 0
    while( i < s.length ){
        if( charHash.containsKey(s[i]) ){
            i = charHash[s[i]]!!.plus(1)
            if( charHash.size > max ){
                max = charHash.size
            }
            charHash.clear()
        }else{
            charHash[s[i]] = i
            if( charHash.size > max ){
                max = charHash.size
            }
            i++
        }
    }
    return max
}

//219. Contains Duplicate II
fun containsNearbyDuplicate(nums: IntArray, k: Int): Boolean {
    val numsHash = mutableMapOf<Int, Int>()
    for ( i in nums.indices ){
        if ( numsHash.containsKey(nums[i]) ){
            if( Math.abs( numsHash[nums[i]]!!.minus(i) ) <= k ){
                return true
            }else{
                numsHash[nums[i]] = i
            }
        }else{
            numsHash[nums[i]] = i
        }
    }
    return false
}

//567. Permutation in String
fun matchHash(s1Hash: Map<Char, Int>, s2Hash: Map<Char, Int>): Boolean{
    for( (k ,v) in s1Hash ){
        if( s2Hash[k] != v){
            return false
        }
    }
    return true
}
fun checkInclusion(s1: String, s2: String): Boolean {
    //create s1 hash
    val s1Hash = mutableMapOf<Char, Int>()
    var windowSize = 0
    for ( c in s1 ){
        s1Hash[c] = s1Hash[c]?.plus(1) ?: 1
        windowSize++
    }

    //slide through s2
    var windowStart = 0
    var windowEnd = 0
    val s2Hash = mutableMapOf<Char, Int>()
    while ( windowEnd < s2.length ){
        //check hash match
        if ( matchHash(s1Hash, s2Hash) ){
            return true
        }
        //initial window
        if ( windowEnd < windowSize ){
            s2Hash[s2[windowEnd]] = s2Hash[s2[windowEnd]]?.plus(1) ?: 1
            windowEnd++
        }else{
            //pop window start
            s2Hash[s2[windowStart]] = s2Hash[s2[windowStart]]!!.minus(1)
            //add window end
            s2Hash[s2[windowEnd]] = s2Hash[s2[windowEnd]]?.plus(1) ?: 1
            //slide window
            windowStart++
            windowEnd++

        }
    }
    if ( matchHash(s1Hash, s2Hash) ){
        return true
    }
    return false
}

//733. Flood Fill
fun floodFill(image: Array<IntArray>, sr: Int, sc: Int, newColor: Int): Array<IntArray> {
    val toChangeColor = image[sr][sc]
    if ( toChangeColor != newColor ){
        val changePoint = mutableListOf<Pair<Int, Int>>()
        changePoint.add(Pair(sr,sc))
        while ( changePoint.size > 0 ){
            val (i, j) = changePoint.removeAt(0)
            if( image[i][j] == toChangeColor ){
                image[i][j] = newColor
                if (i-1 >= 0 && image[i-1][j] == toChangeColor){
                    changePoint.add(Pair(i-1,j))
                }
                if (j-1 >= 0 && image[i][j-1] == toChangeColor){
                    changePoint.add(Pair(i,j-1))
                }
                if (i+1 < image.size && image[i+1][j] == toChangeColor){
                    changePoint.add(Pair(i+1,j))
                }
                if (j+1 < image[0].size && image[i][j+1] == toChangeColor){
                    changePoint.add(Pair(i,j+1))
                }
            }
        }
    }
    return image
}

//695. Max Area of Island
fun checkedIsland(image: Array<IntArray>, sr: Int, sc: Int, newColor: Int): Int {
    var count = 0
    val toChangeColor = 1
    if ( toChangeColor != newColor ){
        val changePoint = mutableListOf<Pair<Int, Int>>()
        changePoint.add(Pair(sr,sc))
        while ( changePoint.size > 0 ){
            val (i, j) = changePoint.removeAt(0)
            if( image[i][j] == toChangeColor ){
                image[i][j] = newColor
                count++
                if (i-1 >= 0 && image[i-1][j] == toChangeColor){
                    changePoint.add(Pair(i-1,j))
                }
                if (j-1 >= 0 && image[i][j-1] == toChangeColor){
                    changePoint.add(Pair(i,j-1))
                }
                if (i+1 < image.size && image[i+1][j] == toChangeColor){
                    changePoint.add(Pair(i+1,j))
                }
                if (j+1 < image[0].size && image[i][j+1] == toChangeColor){
                    changePoint.add(Pair(i,j+1))
                }
            }
        }
    }
    return count
}
fun maxAreaOfIsland(grid: Array<IntArray>): Int {
    val island = 1
    var max = 0
    for ( i in grid.indices ){
        for ( j in grid[i].indices ){
            if ( grid[i][j] == island ){
                val islandSize = checkedIsland(grid, i, j, 2)
                max = Math.max(max, islandSize)
            }
        }
    }
    return max
}

//1207. Unique Number of Occurrences
fun uniqueOccurrences(arr: IntArray): Boolean {
    val countHash = mutableMapOf<Int, Int>()
    for ( num in arr ){
        countHash[num] = countHash[num]?.plus(1) ?: 1
    }
    val occurSet = HashSet(countHash.values)
    if( occurSet.size != countHash.size )
        return false
    return true
}

//26. Remove Duplicates from Sorted Array
fun removeDuplicates(nums: IntArray): Int {
    var left = 0
    var right = 0
    while( right < nums.size-1 ){
        if( nums[right] != nums[right+1] ){
            nums[++left] = nums[++right]
        }else{
            right++
        }
    }
    return left+1
}

//27. Remove Element
fun removeElement(nums: IntArray, k: Int): Int {
    var left = 0
    var right = 0
    while( right < nums.size ){
         if ( nums[left] != k ){
             left++
             right = left
         }else{
            if( nums[right] != k ){
                nums[left] = nums[right]
                nums[right] = k
                left++
                right++
            }else{
                right++
            }
         }
    }
    return left
}

//66. Plus One
fun plusOne(digits: IntArray): IntArray {
    var output = mutableListOf<Int>()
    var carry = 0
    for ( i in digits.size - 1 downTo 0){
        if ( digits[i] < 9 ){
            output.add(0, digits[i] + 1)
        }else{
            output.add(0,0)
        }
    }
    return output.toIntArray()
}

//349. Intersection of Two Arrays
fun intersection(nums1: IntArray, nums2: IntArray): IntArray {
    val countHash = mutableMapOf<Int,Int>()
    for( num in nums1 ){
        if( !countHash.containsKey(num) ){
            countHash[num] = 0
        }
    }
    val outList = mutableListOf<Int>()
    for( num in nums2 ){
        if( countHash.containsKey(num) ){
            outList.add(num)
            countHash.remove(num)
        }
    }

    return outList.toIntArray()
}

//617. Merge Two Binary Trees
class TreeNode(var `val`: Int) {
    var left: TreeNode? = null
    var right: TreeNode? = null
}
fun mergeTrees(root1: TreeNode?, root2: TreeNode?): TreeNode? {
    if ( root1 == null ) return root2
    if ( root2 == null ) return root1
    val root1Node = mutableListOf<TreeNode>()
    val root2Node = mutableListOf<TreeNode>()
    root1Node.add(root1)
    root2Node.add(root2)
    while ( root1Node.isNotEmpty() && root2Node.isNotEmpty() ){
        val r1 = root1Node.removeAt(0)
        val r2 = root2Node.removeAt(0)
        r1.`val` += r2.`val`
        //left
        if ( r1.left == null && r2.left != null ){
            r1.left = r2.left
        }else if ( r1.left != null && r2.left == null){

        }else{
            r1.left?.let { root1Node.add(it) }
            r2.left?.let { root2Node.add(it) }
        }
        //right
        if ( r1.right == null && r2.right != null ){
            r1.right = r2.right
        }else if ( r1.right != null && r2.right == null){

        }else{
            r1.right?.let { root1Node.add(it) }
            r2.right?.let { root2Node.add(it) }
        }
    }


    return root1
}

//116. Populating Next Right Pointers in Each Node
class Node(var `val`: Int) {
    var left: Node? = null
    var right: Node? = null
    var next: Node? = null
}
fun connect(root: Node?): Node? {
    if ( root == null ) return null
    val nodeList = mutableListOf<Node?>()
    nodeList.add(root)
    var count = 1
    var popCount = 0
    var tempNode: Node? = null
    while ( nodeList.isNotEmpty() ){
        while ( popCount < count ){
            val rightNode = nodeList.removeAt(0)
            rightNode!!.next = tempNode
            tempNode = rightNode
            if( rightNode.right != null ){
                nodeList.add(rightNode.right)
            }
            if ( rightNode.left != null ){
                nodeList.add(rightNode.left)
            }
            popCount++
        }
        count *= 2
        popCount = 0
        tempNode = null
    }
    return root
}

//345. Reverse Vowels of a String
fun isVowel(c: Char): Boolean{
    if ( c == 'a' || c == 'e' || c == 'i' || c == 'o' || c == 'u'  ){
        return true
    }
    if ( c == 'A' || c == 'E' || c == 'I' || c == 'O' || c == 'U'  ){
        return true
    }
    return false
}
fun reverseVowels(s: String): String {
    var start = 0
    var end = s.length-1
    val outputBuilder = StringBuilder()
    outputBuilder.append(s)
    while ( start < end ){
        val c1 = outputBuilder[start]
        val c2 = outputBuilder[end]
        if ( isVowel(c1) && isVowel(c2) ) {
            outputBuilder[start] = c2
            outputBuilder[end] = c1
            start++
            end--
        }
        if ( !isVowel(c1) ){
            start++
        }
        if ( !isVowel(c2) ){
            end--
        }
    }
    return outputBuilder.toString()
}

//542. 01 Matrix
fun updateMatrix(mat: Array<IntArray>): Array<IntArray> {
    val rowSize = mat.size
    val colSize = mat[0].size
    val output = Array(rowSize){ IntArray(colSize){Int.MAX_VALUE-10000} }
    //left and top
    for ( row in 0 until rowSize ){
        for ( col in 0 until colSize){
            if ( mat[row][col] == 0 ){
                output[row][col] = 0
            }else{
                if ( row > 0 ){
                    output[row][col] = Math.min(output[row][col], output[row-1][col]+1)
                }
                if ( col > 0 ){
                    output[row][col] = Math.min(output[row][col], output[row][col-1]+1)
                }
            }
        }
    }

    //right and bottom
    for ( row in rowSize-1 downTo 0){
        for ( col in colSize-1 downTo 0){
            if ( mat[row][col] == 0 ){
                output[row][col] = 0
            }else{
                if ( row + 1 < rowSize ){
                    output[row][col] = Math.min(output[row][col], output[row+1][col]+1)
                }
                if ( col + 1 < colSize){
                    output[row][col] = Math.min(output[row][col], output[row][col+1]+1)
                }
            }

        }
    }

    return output
}

//994. Rotting Oranges
fun orangesRotting(grid: Array<IntArray>): Int {
    var count = 0
    val empty = 0
    val fresh = 1
    val bad = 2
    val badList = mutableListOf<Pair<Int,Int>>()
    val rowSize = grid.size
    val colSize = grid[0].size
    var goodCount = 0
    //find bad
    for ( row in 0 until rowSize ){
        for ( col in 0 until colSize ){
            if ( grid[row][col] == bad ){
                badList.add(Pair(row,col))
            }else if ( grid[row][col] == fresh){
                goodCount++
            }
        }
    }
    //no bad
    if ( goodCount == 0 ){
        return 0
    }
    val dirs = arrayOf(intArrayOf(1, 0), intArrayOf(-1, 0), intArrayOf(0, 1), intArrayOf(0, -1))
    while ( badList.isNotEmpty() ){
        val size = badList.size
        val prevGoodCount = goodCount
        count++
        for ( i in 0 until size ){
            val ( row, col ) = badList.removeAt(0)
            for ( dir in dirs ){
                val x = row + dir[0]
                val y = col + dir[1]
                if ( x < 0 || x >= rowSize || y < 0 || y >= colSize || grid[x][y] == empty || grid[x][y] == bad){
                    continue
                }
                if ( grid[x][y] == fresh){
                    grid[x][y] = bad
                    badList.add(Pair(x,y))
                    goodCount--
                }
            }
        }
    }
    if ( goodCount > 0 ){
        return -1
    }
    return count-1

}

//917. Reverse Only Letters
fun reverseOnlyLetters(s: String): String {
    var left = 0
    var right = s.length-1
    val outputBuilder = StringBuilder()
    outputBuilder.append(s)
    while ( left < right ){
        val c1 = outputBuilder[left]
        val c2 = outputBuilder[right]
        if ( !c1.isLetter() ){
            left++
        }
        if ( !c2.isLetter() ){
            right--
        }
        if ( c1.isLetter() && c2.isLetter() ){
            outputBuilder[left] = c2
            outputBuilder[right] = c1
            left++
            right--
        }
    }
    return outputBuilder.toString()
}

//220. Contains Duplicate III
fun containsNearbyAlmostDuplicate(nums: IntArray, k: Int, t: Int): Boolean {
    val numHash = mutableMapOf<Int,Int>()
    var windowStart = 0
    var windowEnd = 0
    while ( windowEnd < nums.size ){
        if ( windowEnd <= k ){

        }
    }
    return false
}

//21. Merge Two Sorted Lists
fun mergeTwoLists(l1: ListNode?, l2: ListNode?): ListNode? {
    var head1 = l1
    var head2 = l2
    val outList = ListNode(0)
    var curNode = outList
    while ( head1 != null && head2 != null){
        if ( head1.`val` <= head2.`val`){
            curNode.next = head1
            curNode = curNode.next!!
            head1 = head1.next
        }else{
            curNode.next = head2
            curNode = curNode.next!!
            head2 = head2.next
        }
    }
    if ( head1 == null ){
        curNode.next = head2
    }else{
        curNode.next = head1
    }
    return outList.next
}

//206. Reverse Linked List
fun reverseList(head: ListNode?): ListNode? {
    val nodeStack = mutableListOf<ListNode>()
    var curNode = head
    while ( curNode != null ){
        nodeStack.add(curNode)
        curNode = curNode.next
    }
    var outHead : ListNode? = null
    var curOutHead = outHead
    while ( nodeStack.isNotEmpty() ){
        val temp = nodeStack.removeAt(nodeStack.size-1)
        if ( outHead == null ){
            outHead = temp
            curOutHead = temp
        }else{
            curOutHead!!.next = temp
            curOutHead = temp
            curOutHead.next = null
        }
    }
    return outHead
}

//217. Contains Duplicate
fun containsDuplicate(nums: IntArray): Boolean {
    val numHashSet = mutableSetOf<Int>()
    for ( num in nums ){
        if ( numHashSet.contains(num) ){
            return true
        }else{
            numHashSet.add(num)
        }
    }
    return false
}

//350. Intersection of Two Arrays II
fun intersect(nums1: IntArray, nums2: IntArray): IntArray {
    val numHash = mutableMapOf<Int,Int>()
    for ( num in nums1 ){
        numHash[num] = numHash[num]?.plus(1) ?: 1
    }
    val outPut = mutableListOf<Int>()
    for ( num in nums2 ){
        if ( numHash.containsKey(num) && numHash[num]!! > 0 ){
            outPut.add(num)
            numHash[num] = numHash[num]!! - 1
        }
    }
    return outPut.toIntArray()
}

//121. Best Time to Buy and Sell Stock
fun maxProfit(prices: IntArray): Int {
    var minVal = Integer.MAX_VALUE
    var maxProfit = 0
    for ( i in prices.indices ){
        if ( prices[i] < minVal ){
            minVal = prices[i]
        }else if ( prices[i] - minVal > maxProfit  ){
            maxProfit = prices[i] - minVal
        }
    }
    return maxProfit
}

//566. Reshape the Matrix
fun matrixReshape(mat: Array<IntArray>, r: Int, c: Int): Array<IntArray> {
    val rowSize = mat.size
    val colSize = mat[0].size
    if ( rowSize * colSize != r * c){
        return mat
    }
    val outPut = Array(r){ IntArray(c){-1} }
    var count = 0
    for ( i in 0 until rowSize ){
        for ( j in 0 until colSize ){
            outPut[count/c][count%c] = mat[i][j]
            count++
        }
    }
    return outPut
}

//118. Pascal's Triangle
fun generate(numRows: Int): List<List<Int>> {
    val outPut = mutableListOf<List<Int>>()
    for ( i in 0 until numRows ){
        println(i)
        if ( i == 0 ){
            val tempList = mutableListOf<Int>()
            tempList.add(1)
            outPut.add(tempList)
        }else if ( i == 1) {
            val tempList = mutableListOf<Int>()
            tempList.add(1)
            tempList.add(1)
            outPut.add(tempList)
        }else{
            val prevList = outPut[i-1]
            val tempList = mutableListOf<Int>()
            tempList.add(1)
            for ( j in 1 until i ){
                tempList.add(prevList[j]+prevList[j-1])
            }
            tempList.add(1)
            outPut.add(tempList)
        }
    }
    return outPut
}

//387. First Unique Character in a String
fun firstUniqChar(s: String): Int {
    val charHash = mutableMapOf<Char,Int>()
    for ( c in s ){
        charHash[c] = charHash[c]?.plus(1) ?: 1
    }
    for ( i in s.indices ){
        if( charHash[s[i]] == 1 ){
            return i
        }
    }
    return -1
}

//383. Ransom Note
fun canConstruct(ransomNote: String, magazine: String): Boolean {
    val charHash = mutableMapOf<Char,Int>()
    for ( c in magazine ){
        charHash[c] = charHash[c]?.plus(1) ?: 1
    }
    for ( c in ransomNote ){
        if ( charHash[c] ==  1){
            charHash.remove(c)
        }else if ( charHash.containsKey(c) ){
            charHash[c] = charHash[c]!!.minus(1)
        }else{
            return false
        }
    }
    return true
}

//242. Valid Anagram
fun isAnagram(s: String, t: String): Boolean {
    val charHash = mutableMapOf<Char,Int>()
    for( c in s ){
        charHash[c] = charHash[c]?.plus(1) ?: 1
    }
    for ( c in t ){
        if ( charHash[c] == 1 ){
            charHash.remove(c)
        }else if ( charHash.containsKey(c) ){
            charHash[c] = charHash[c]!!.minus(1)
        }else{
            return false
        }
    }
    if ( charHash.isNotEmpty() ){
        return false
    }
    return true
}

//141. Linked List Cycle
fun hasCycle(head: ListNode?): Boolean {
    if (head == null){
        return false
    }
    var slowPointer = head
    var fastPointer = head
    while (fastPointer!!.next != null && fastPointer.next!!.next != null) {
        slowPointer = slowPointer!!.next
        fastPointer = fastPointer.next!!.next
        if (slowPointer == fastPointer) {
            return true
        }
    }
    return false
}

//203. Remove Linked List Elements
fun removeElements(head: ListNode?, k: Int): ListNode? {
    val output = ListNode(0)
    output.next = head
    var curNode = output
    while ( curNode != null && curNode.next != null ){
        if ( curNode.next!!.`val` == k ){
            curNode.next = curNode.next!!.next
        }else{
            curNode = curNode.next!!
        }
    }
    return output.next
}

//88. Merge Sorted Array
fun merge(nums1: IntArray, m: Int, nums2: IntArray, n: Int): Unit {
    var i = m-1
    var j = n-1
    var curPos = m + n -1
    while ( i >= 0 && j >= 0 ){
        if ( nums1[i] >= nums2[j]){
            nums1[curPos] = nums1[i]
            i--
        }else{
            nums1[curPos] = nums2[j]
            j--
        }
        curPos--
    }
    while ( j >= 0 ){
        nums1[curPos] = nums2[j]
        curPos--
        j--
    }
}

//53. Maximum Subarray
fun maxSubArray(nums: IntArray): Int {
    if ( nums.size == 1 ){
        return nums[0]
    }
    var max = nums[0]
    val sumList = IntArray(nums.size)
    sumList[0] = nums[0]
    for ( i in 1 until sumList.size ){
        sumList[i] = Math.max(nums[i], nums[i]+sumList[i-1])
        max = Math.max( sumList[i], max )
    }
    println(nums.toList())
    println(sumList.toList())
    return max
}

//36. Valid Sudoku
fun isValidSudoku(board: Array<CharArray>): Boolean {
    for (i in 0..8) {
        val rows = HashSet<Char>()
        val columns = HashSet<Char>()
        val cube = HashSet<Char>()
        for (j in 0..8) {
            if (board[i][j] != '.' && !rows.add(board[i][j])) return false
            if (board[j][i] != '.' && !columns.add(board[j][i])) return false
            val rowIndex = 3 * (i / 3)
            val colIndex = 3 * (i % 3)
            if (board[rowIndex + j / 3][colIndex + j % 3] != '.' && !cube.add(board[rowIndex + j / 3][colIndex + j % 3])) return false
        }
    }
    return true
}

//83. Remove Duplicates from Sorted List
fun deleteDuplicates(head: ListNode?): ListNode? {
    if( head == null ){
        return null
    }
    var slowPointer = head
    var fastPointer = head!!.next
    while ( slowPointer != null && fastPointer != null ){
        if ( slowPointer.`val` == fastPointer.`val` ){
            fastPointer = fastPointer.next
        } else{
            slowPointer.next = fastPointer
            slowPointer = fastPointer
            fastPointer = fastPointer.next
        }
        if ( fastPointer == null ){
            slowPointer.next = fastPointer
            slowPointer = fastPointer
        }
    }
    return head
}

//144. Binary Tree Preorder Traversal
fun preorderTraversal(root: TreeNode?): List<Int> {
    val outputList = mutableListOf<Int>()
    val nodeList = mutableListOf<TreeNode>()
    if (root != null ){
        nodeList.add(root)
    }
    while ( nodeList.isNotEmpty() ){
        val curNode = nodeList.removeAt(nodeList.size-1)
        outputList.add(curNode.`val`)
        if ( curNode.right != null ){
            nodeList.add(curNode.right!!)
        }
        if ( curNode.left != null ){
            nodeList.add(curNode.left!!)
        }
    }

    return outputList
}

//94. Binary Tree Inorder Traversal
fun inorderTraversal(root: TreeNode?): List<Int> {
    val outputList = mutableListOf<Int>()
    val nodeStack = Stack<TreeNode>()
    var currNode = root
    while (  currNode != null || nodeStack.isNotEmpty() ){
        while ( currNode != null ){
            nodeStack.push(currNode)
            currNode = currNode.left
        }
        currNode = nodeStack.pop()
        outputList.add(currNode.`val`)
        currNode = currNode.right
    }
    return outputList
}

//145. Binary Tree Postorder Traversal
fun postorderTraversal(root: TreeNode?): List<Int> {
    val outputList = mutableListOf<Int>()
    val nodeStack = Stack<TreeNode>()
    if ( root != null ){
        nodeStack.push(root)
    }
    while ( nodeStack.isNotEmpty() ){
        val curNode = nodeStack.pop()
        outputList.add(0, curNode.`val`)
        if ( curNode.left != null ){
            nodeStack.push(curNode.left)
        }
        if ( curNode.right != null ){
            nodeStack.push(curNode.right)
        }
    }
    return outputList
}

//102. Binary Tree Level Order Traversal
fun levelOrder(root: TreeNode?): List<List<Int>> {
    val outputList = mutableListOf<List<Int>>()
    val nodeList = mutableListOf<TreeNode>()
    if ( root != null ){
        nodeList.add(root)
    }
    while ( nodeList.isNotEmpty() ){
        val listSize = nodeList.size
        val levelList = mutableListOf<Int>()
        for ( i in 0 until listSize ){
            val curNode = nodeList.removeAt(0)
            levelList.add(curNode.`val`)
            if ( curNode.left != null ){
                nodeList.add(curNode.left!!)
            }
            if ( curNode.right != null ){
                nodeList.add(curNode.right!!)
            }
        }
        outputList.add(levelList)
    }
    return outputList
}

//104. Maximum Depth of Binary Tree
fun maxDepth(root: TreeNode?): Int {
    var depth = 0
    val nodeList = mutableListOf<TreeNode>()
    if ( root != null ){
        nodeList.add(root)
    }
    while ( nodeList.isNotEmpty() ){
        val listSize = nodeList.size
        for ( i in 0 until listSize ){
            val curNode = nodeList.removeAt(0)
            if ( curNode.left != null ){
                nodeList.add(curNode.left!!)
            }
            if ( curNode.right != null ){
                nodeList.add(curNode.right!!)
            }
        }
        depth++
    }
    return depth
}

//226. Invert Binary Tree
fun invertTree(root: TreeNode?): TreeNode? {
    val nodeStack = Stack<TreeNode>()
    if ( root != null ){
        nodeStack.push(root)
    }
    while ( nodeStack.isNotEmpty() ){
        val curNode = nodeStack.pop()
        val leftNode = curNode.left
        val rightNode = curNode.right
        //invert left & right
        curNode.left = rightNode
        curNode.right = leftNode
        if ( leftNode != null ){
            nodeStack.push(leftNode)
        }
        if ( rightNode != null ){
            nodeStack.push(rightNode)
        }
    }
    return root
}

//112. Path Sum
fun hasPathSum(root: TreeNode?, targetSum: Int): Boolean {
    var sum = 0
    val nodeStack = Stack<TreeNode>()
    var curNode = root
    if ( root != null ){
        nodeStack.push(root)
    }
    while ( curNode != null || nodeStack.isNotEmpty() ){
        while ( curNode != null ){
            sum += curNode.`val`
            curNode = curNode.left
            if( curNode != null ){
                nodeStack.push(curNode)
            }
        }
    }
    return false
}

//700. Search in a Binary Search Tree
fun searchBST(root: TreeNode?, target: Int): TreeNode? {
    var curNode = root
    while ( curNode != null ){
        if ( curNode.`val` == target ){
            return curNode
        }else if ( target > curNode.`val` ){
            curNode = curNode.right
        }else{
            curNode = curNode.left
        }
    }
    return curNode
}

//701. Insert into a Binary Search Tree
fun insertIntoBST(root: TreeNode?, k: Int): TreeNode? {
    if ( root == null ){
        return TreeNode(k)
    }
    var curNode = root
    while ( curNode != null ){
        if ( k > curNode.`val` && curNode.right != null ){
            curNode = curNode.right
        }else if ( k > curNode.`val` && curNode.right == null ){
            curNode.right = TreeNode(k)
            break
        }else if ( k < curNode.`val` && curNode.left != null ){
            curNode = curNode.left
        }else if ( k < curNode.`val` && curNode.left == null ){
            curNode.left = TreeNode(k)
            break
        }
    }
    return root
}

//653. Two Sum IV - Input is a BST
fun findTarget(root: TreeNode?, k: Int): Boolean {
    val nodeList = mutableListOf<TreeNode>()
    if ( root != null ){
        nodeList.add(root)
    }
    val numSet = mutableSetOf<Int>()
    while ( nodeList.isNotEmpty() ){
        val curNode = nodeList.removeAt(0)
        if ( numSet.contains(k-curNode.`val`) ){
            return true
        }else{
            numSet.add(curNode.`val`)
            if ( curNode.left != null ){
                nodeList.add(curNode.left!!)
            }
            if ( curNode.right != null ){
                nodeList.add(curNode.right!!)
            }
        }
    }
    return false
}

//98. Validate Binary Search Tree
fun isValidBST(root: TreeNode?): Boolean {
    val nodeList = mutableListOf<TreeNode>()
    var min = 0
    var max = 0
    if ( root != null ){
        nodeList.add(root)
        min = root.`val`
        max = root.`val`
    }
    while ( nodeList.isNotEmpty() ){
        val curNode = nodeList.removeAt(0)
        if ( curNode.left != null ){
            val leftNode = curNode.left!!
            if ( leftNode.`val` < curNode.`val` && leftNode.`val` < max ){
                nodeList.add(leftNode)
            }else{
                return false
            }
        }
        if ( curNode.right != null ){
            val rightNode = curNode.right!!
            if ( rightNode.`val` > curNode.`val` && rightNode.`val` > min ){
                nodeList.add(rightNode)
            }else{
                return false
            }
        }
    }
    return true
}

//1619. Mean of Array After Removing Some Elements
fun trimMean(arr: IntArray): Double {
    val removeSize = (arr.size * 0.05).toInt()
    return arr.sorted().subList(removeSize, arr.size-removeSize).sum() / (arr.size - removeSize*2).toDouble()
}

//2085. Count Common Words With One Occurrence
fun countWords(words1: Array<String>, words2: Array<String>): Int {
    var count = 0
    val groupHash1 = mutableMapOf<String, Int>()
    val groupHash2 = mutableMapOf<String, Int>()
    for ( word in words1 ){
        groupHash1[word] = groupHash1[word]?.plus(1) ?: 1
    }
    for ( word in words2 ){
        groupHash2[word] = groupHash2[word]?.plus(1) ?: 1
    }
    for ( (key, value) in groupHash1 ){
        if( value == 1 && groupHash2[key] ==1 ){
            count++
        }
    }
    return count
}

//476. Number Complement
fun findComplement(num: Int): Int {
    var current = num
    var sum = 0
    var count = 0

    while ( current > 0 ){
        val remainder = current % 2
        if ( remainder == 0 ){
            sum += 1 * 2.0.pow(count.toDouble()).toInt()
        }
        count++
        current /= 2
        if ( current == 1) break
    }

    return sum
}

//1936. Add Minimum Number of Rungs
fun addRungs(rungs: IntArray, dist: Int): Int {
    var add = 0
    var i = 0
    var current = 0

    while ( i < rungs.size ){
        val next = rungs[i]
        if( next - current > dist ){
             add += if( (next - current) % dist == 0 ){
                (next - current)/dist - 1
            }else{
                (next - current)/dist
            }
        }
        current = rungs[i++]
    }
    return add
}

//2114. Maximum Number of Words Found in Sentences
fun mostWordsFound(sentences: Array<String>): Int {
    var max = 0
    for ( sentence in sentences ){
        max = Math.max(max, sentence.split(" ").size)
    }
    return max
}

//2023. Number of Pairs of Strings With Concatenation Equal to Target
fun numOfPairs(nums: Array<String>, target: String): Int {
    var count = 0
    for ( i in nums.indices){
        for ( j in i+1 until nums.size){
            if ( nums[i] + nums[j] == target) {
                count++
            }
            if ( nums[j] + nums[i] == target ){
                count++
            }
        }
    }
    return count
}

//2165. Smallest Value of the Rearranged Number
fun smallestNumber(num: Long): Long {
    if ( num == 0L) return 0L
    val isPositive = num >= 0
    val numString = num.toString()
    val numBucket = MutableList(10){0}
    for ( i in numString.length-1 downTo  0){
        if ( !isPositive && i == 0){
            break
        }
        numBucket[numString[i].digitToInt()]++
    }
    var resultString = java.lang.StringBuilder()
    if ( isPositive ){
        var addFirst = false
        for ( i in 1 until numBucket.size){
            for ( j in 0 until numBucket[i] ){
                if ( !addFirst ){
                    resultString.append(i.toString())
                    for ( k in 0 until numBucket[0]){
                        resultString.append(0)
                    }
                    addFirst = true
                }else{
                    resultString.append(i.toString())
                }
            }
        }
    }else{
        resultString.append("-")
        for ( i in numBucket.size - 1 downTo 0 ){
            for ( j in 0 until numBucket[i] ){
                resultString.append(i.toString())
            }
        }
    }
    return resultString.toString().toLong()
}

//1834. Single-Threaded CPU
fun getOrder(tasks: Array<IntArray>): IntArray {
    class CPUTask(val taskId : Int, val enqueueTime : Int, val processTime : Int): Comparable<CPUTask>{
        override fun compareTo(other: CPUTask): Int {
            return if ( this.processTime == other.processTime){
                this.taskId - other.taskId
            }else{
                this.processTime - other.processTime
            }
        }
    }

    val taskList = mutableListOf<CPUTask>()
    for ( i in tasks.indices){
        taskList.add(CPUTask(i, tasks[i][0], tasks[i][1]))
    }
    taskList.sortBy { it.enqueueTime }
    val pq = PriorityQueue<CPUTask>()
    val result = mutableListOf<Int>()
    var currentTime = 0
    var taskPointer = 0
    while ( result.size < taskList.size ){
        //add task to pq
        while ( taskPointer < taskList.size && currentTime>=taskList[taskPointer].enqueueTime){
            pq.offer(taskList[taskPointer++])
        }
        //when pq is empty, set time to next enqueue time
        if ( pq.isEmpty() ){
            currentTime = taskList[taskPointer].enqueueTime
            continue
        }

        //get task from pq
        val curTask = pq.poll()
        currentTime += curTask.processTime
        result.add(curTask.taskId)
    }
    return result.toIntArray()
}

//583. Delete Operation for Two Strings
fun minDistance(word1: String, word2: String): Int {
    val dp = Array(word1.length + 1) { IntArray(word2.length + 1) }
    for ( i in 0..word1.length ){
        for ( j in 0..word2.length ){
            if ( i == 0 || j == 0){
                dp[i][j] = i + j
            }else if ( word1[i-1] == word2[j-1] ){
                dp[i][j] = dp[i-1][j-1]
            }else{
                dp[i][j] = Math.min(dp[i-1][j], dp[i][j-1]) + 1
            }
        }
    }
    return dp[word1.length][word2.length]
}

//70. Climbing Stairs
fun climbStairs(n: Int): Int {
    val dp = Array(n+1){0}
    val oneStep = 1
    val twoStep = 2
    for ( i in dp.indices ){
        if ( i + oneStep <= n ){
            if ( i == 0 ){
                dp[oneStep] = 1
            }
            dp[i+oneStep] += dp[i]
        }
        if ( i + twoStep <= n){
            if ( i == 0){
                dp[twoStep] = 1
            }
            dp[i+twoStep] += dp[i]
        }
    }
    return dp[n]
}

//392. Is Subsequence
fun isSubsequence(s: String, t: String): Boolean {
    if (s.length > t.length ) return false

    var sPointer = 0
    var tPointer = 0

    while ( sPointer < s.length && tPointer < t.length ){
        if ( s[sPointer] == t[tPointer] ){
            sPointer++
            tPointer++
        }else{
            tPointer++
        }
    }

    return sPointer == s.length
}

//119. Pascal's Triangle II
fun getRow(rowIndex: Int): List<Int> {
    if ( rowIndex == 0 ) return listOf(1)
    if ( rowIndex == 1 ) return listOf(1,1)

    var ar = List(2){1}.toMutableList()
    for ( curRow in 2..rowIndex ){
        val oldAr = ar
        val newAr = List(curRow+1){1}.toMutableList()
        for ( i in 1 until curRow ){
            newAr[i] = oldAr[i] + oldAr[i-1]
        }
        ar = newAr
    }

    return ar
}

//922. Sort Array By Parity II
fun sortArrayByParityII(nums: IntArray): IntArray {
    var evenPointer = 0
    var oddPointer = 1
    val swap = { array: IntArray, i : Int, j : Int ->
        val temp = array[i]
        array[i] = array[j]
        array[j] = temp
    }
    while ( evenPointer < nums.size && oddPointer < nums.size ){

        while ( evenPointer < nums.size && nums[evenPointer] % 2 == 0 ){ //Even
            evenPointer += 2
        }

        while ( oddPointer < nums.size && nums[oddPointer] % 2 == 1 ){ //Odd
            oddPointer += 2
        }

        if ( evenPointer < nums.size && oddPointer < nums.size){
            swap(nums, evenPointer, oddPointer)
        }

    }

    return nums
}

//746. Min Cost Climbing Stairs
fun minCostClimbingStairs(cost: IntArray): Int {
    //first solution
    //add 1 to act as top
    //dp[cost.size] -> final min cost
//        val dp = Array(cost.size+1){ if ( it <= 1 ) 0 else Integer.MAX_VALUE }
//
//        for ( i in cost.indices){
//            //i+1
//            dp[i+1] = Math.min(dp[i+1], dp[i]+cost[i])
//
//            //i+2
//            if ( i + 2 <= cost.size ){
//                dp[i+2] = Math.min(dp[i+2], dp[i]+cost[i])
//            }
//        }
//
//        return dp[cost.size]

    //optimize above solution
    var first = 0
    var second = 0
    for ( i in cost.indices){

        //i+1
        val temp = Math.min(second, first+cost[i])

        //i+2
        second = first+cost[i]

        //swap
        first = temp
    }

    return first
}


//2011. Final Value of Variable After Performing Operations
fun finalValueAfterOperations(operations: Array<String>): Int {
//        var result = 0
//        for ( operation in operations ){
//            if ( operation.contains("++") ){
//                result++
//            }else{
//                result--
//            }
//        }
//        return result

//        kotlin optimization
    return operations.fold(0){ sum, element ->
        if ( element.contains("++") ){
            sum+1
        }else{
            sum-1
        }
    }
}

//946. Validate Stack Sequences
fun validateStackSequences(pushed: IntArray, popped: IntArray): Boolean {
    val stack = Stack<Int>()
    var poppedPointer = 0

    for ( pushElement in pushed ){
        stack.push(pushElement)
        while ( poppedPointer < popped.size && !stack.empty() && stack.peek() == popped[poppedPointer] ){
            stack.pop()
            poppedPointer++
        }
    }

    return poppedPointer == popped.size
}

//7. Reverse Integer
fun reverse(x: Int): Int {
    var currentVal = x
    var result = 0

    while ( currentVal != 0){
        val newDigit = currentVal % 10
        currentVal /= 10
        if ( result > Int.MAX_VALUE / 10 || ( result == Int.MAX_VALUE / 10 && newDigit > 7 ) ){
            return 0
        }
        if ( result < Int.MIN_VALUE / 10 || ( result == Int.MIN_VALUE / 10 && newDigit < -8) ){
            return 0
        }
        result = result * 10 + newDigit
    }

    return result
}


//316. Remove Duplicate Letters
fun removeDuplicateLetters(s: String): String {
    val hasVisit = Array(26){false}
    val count = Array(26){0}
    val ansStack = Stack<Char>()
    val ansStringBuilder = StringBuilder()

    for ( ch in s ){
        count[ch-'a']++
    }

    for ( ch in s ){
        count[ch-'a']--

        if ( hasVisit[ch-'a'] ) {
            continue
        }

        //check smaller, duplicate
        while ( ansStack.isNotEmpty() && ch < ansStack.peek() && count[ansStack.peek()-'a'] > 0 ){
            val pop = ansStack.pop()
            hasVisit[pop-'a'] = false
        }
        ansStack.push(ch)
        hasVisit[ch-'a'] = true
    }

    while ( ansStack.isNotEmpty() ){
        ansStringBuilder.insert(0, ansStack.pop())
    }

    return ansStringBuilder.toString()
}

//674. Longest Continuous Increasing Subsequence
fun findLengthOfLCIS(nums: IntArray): Int {
    var pointer = 0
    var count = 1
    var max = count
    while ( pointer + 1 < nums.size ){
        if ( nums[pointer] < nums[pointer+1] ){
            count++
            max = Math.max(max, count)
        }else{
            count = 1
        }
        pointer++
    }
    return max
}


//1007. Minimum Domino Rotations For Equal Row
fun minDominoRotations(tops: IntArray, bottoms: IntArray): Int {

    // O(n) + O(n) solution
//        val count = Array(6){0}
//        var targetNum = 0
//
//        for ( i in tops.indices ){
//            if ( tops[i] == bottoms[i] ){
//                count[tops[i]-1]++
//            }else{
//                count[tops[i]-1]++
//                count[bottoms[i]-1]++
//            }
//
//            if ( count[tops[i]-1] >= tops.size ){
//                targetNum = tops[i]
//            }
//            if ( count[bottoms[i]-1] >= tops.size ){
//                targetNum = bottoms[i]
//            }
//        }
//
//        if ( targetNum == 0 ){
//            return -1
//        }
//
//        var swapToTop = 0
//        var swapToBottom = 0
//
//        for ( i in tops.indices ){
//            if ( tops[i] == targetNum && bottoms[i] == targetNum ){
//                continue
//            }
//            if ( tops[i] == targetNum ){
//                swapToBottom++
//            }else{
//                swapToTop++
//            }
//        }
//
//        return Math.min(swapToTop, swapToBottom)

    //optimize above
    //O(n) + O(6)
    val sameCount = IntArray(7){0}
    val topCount = IntArray(7){0}
    val bottomCount = IntArray(7){0}
    val length = tops.size

    for ( i in tops.indices ){
        topCount[tops[i]]++
        bottomCount[bottoms[i]]++
        if ( tops[i] == bottoms[i] ){
            sameCount[tops[i]]++
        }
    }

    for ( i in 1 ..6){
        if ( topCount[i] + bottomCount[i] - sameCount[i] >= length ){
            return Math.min(topCount[i], bottomCount[i]) - sameCount[i]
        }
    }

    return -1
}

//763. Partition Labels
fun partitionLabels(s: String): List<Int> {
    //stupid store count
//        val count = Array(26){0}
//        for ( ch in s ){
//            count[ch-'a']++
//        }
//        val resultList = mutableListOf<Int>()
//
//        var startPointer = 0
//        var endPointer = 0
//        var startPos = 0
//        while ( startPointer < s.length  ){
//            val targetChar = s[startPointer]
//            while ( count[targetChar-'a'] > 0 && endPointer < s.length ){
//                val currentChar = s[endPointer]
//                count[currentChar-'a']--
//                endPointer++
//            }
//            startPointer++
//            if ( startPointer == endPointer ){
//                resultList.add(startPointer - startPos)
//                startPos = startPointer
//            }
//        }
//        return resultList

    //store last appear position
    val lastPos = Array(26){0}
    for ( i in s.indices ){
        lastPos[s[i]-'a'] = i
    }
    val resultList = mutableListOf<Int>()
    var startPos = 0
    var endPos = 0
    for ( i in s.indices ){
        endPos = Math.max(endPos, lastPos[s[i]-'a'])
        if ( i == endPos ){
            resultList.add(endPos - startPos + 1)
            startPos = endPos + 1
        }
    }

    return resultList
}

//1663. Smallest String With A Given Numeric Value
fun getSmallestString(n: Int, k: Int): String {
    var remain = k
    var count = n
    val resultStack = Stack<Int>()

    while ( count > 0 ){
        if( count == 1 ){
            resultStack.push(remain)
        }else{
            if ( remain - count + 1 > 26 ){
                resultStack.push(26)
                remain -= 26
            }else{
                resultStack.push(remain-count+1)
                remain -= remain - count + 1
            }
        }
        count--
    }

    val resultSB = StringBuilder()
    while ( resultStack.isNotEmpty() ){
        resultSB.append((resultStack.pop()+96).toChar())
    }
    return resultSB.toString()
}

//991. Broken Calculator
fun brokenCalc(startValue: Int, target: Int): Int {
    var result = 0
    var curTarget = target
    while (curTarget > startValue) {
        if (curTarget % 2 === 0) {
            curTarget /= 2
        } else {
            curTarget++
        }
        result++
    }
    return result + (startValue - curTarget)

}

//881. Boats to Save People
fun numRescueBoats(people: IntArray, limit: Int): Int {
    val sortedPeople = people.sorted()
    var i = 0
    var j: Int = sortedPeople.size - 1
    var result = 0

    while (i <= j) {
        result++
        if (sortedPeople[i] + sortedPeople[j] <= limit) i++
        j--
    }

    return result
}

//2000. Reverse Prefix of Word
fun reversePrefix(word: String, ch: Char): String {
    val index = word.indexOf(ch)
    return if ( index ==-1 ) {
        word
    }else {
        val resultSB = StringBuilder()
        for ( i in index downTo 0){
            resultSB.append(word[i])
        }
        resultSB.toString() + word.substring(index+1)
    }
}

//17. Letter Combinations of a Phone Number
fun letterCombinations(digits: String): List<String> {
    if ( digits.isBlank() ) return emptyList()
    val numPad = listOf("0", "1", "abc", "def", "ghi", "jkl", "mno", "pqrs", "tuv", "wxyz")
    val resultList = mutableListOf<String>()
    resultList.add("")
    while ( resultList[0].length < digits.length ){
        val curString = resultList.removeAt(0)
        for ( ch in numPad[digits[curString.length].digitToInt()] ){
            resultList.add(curString + ch)
        }
    }

    return resultList
}

//81. Search in Rotated Sorted Array II
fun search81(nums: IntArray, target: Int): Boolean {
    return nums.firstOrNull { it == target }?.let{
        true
    }?: false
}

//287. Find the Duplicate Number
// shold use cyclic detection, given that the value itself will not cause out of bound
fun findDuplicate(nums: IntArray): Int {
    val numSet = mutableSetOf<Int>()
    for ( num in nums ){
        if ( numSet.contains(num) ){
            return num
        }else{
            numSet.add(num)
        }
    }
    return -1
}

//1029. Two City Scheduling
fun twoCitySchedCost(costs: Array<IntArray>): Int {
    //the acutal profit parameter is the benefit if we chose to go to either one city
    costs.sortWith { o1, o2 ->
        (o1[0] - o1[1]) - (o2[0] - o2[1])
    }
    return costs.foldIndexed(0) { index, acc, ints ->
        if (index < costs.size / 2) acc + ints[0] else acc + ints[1]
    }
}

//74. Search a 2D Matrix
fun searchMatrix(matrix: Array<IntArray>, target: Int): Boolean {
    //search row
    var head = 0
    var tail = matrix.size-1
    var middle: Int
    var temp: Int
    while ( head <= tail){
        middle = ( head + tail ) / 2
        temp = matrix[middle][0]
        if( temp == target ){ //find target
            return true
        }else if ( temp < target ){ // target on bottom side
            head = middle + 1
        }
        else{ // target on top side
            tail = middle - 1
        }
    }
    if( tail < 0 ){
        return false
    }
    println(head)
    //search in a row
    val rowNum = head-1
    head = 0
    tail = matrix[0].size-1
    while ( head <= tail){
        middle = ( head + tail ) / 2
        temp = matrix[rowNum][middle]
        if( temp == target ){ //find target
            return true
        }else if ( temp < target ){ // target on right side
            head = middle + 1
        }
        else{ // target on left side
            tail = middle - 1
        }
    }
    return false
}

//1721. Swapping Nodes in a Linked List
fun swapNodes(head: ListNode?, k: Int): ListNode? {
    var firstNode : ListNode? = null
    var secondNode : ListNode? = null
    var curNode = head
    var count = k
    while ( curNode != null ){
        if ( secondNode != null ){
            secondNode = secondNode.next
        }
        if ( --count == 0 ){
            firstNode = curNode
            secondNode = head
        }
        curNode = curNode.next
    }
    val temp = firstNode!!.`val`
    firstNode.`val` = secondNode!!.`val`
    secondNode.`val` = temp
    return head
}

//2. Add Two Numbers
fun addTwoNumbers(l1: ListNode?, l2: ListNode?): ListNode? {
    var node1 = l1
    var node2 = l2
    val tempHead = ListNode(-1)
    var curNode = tempHead
    var carrier = 0
    while ( !(node1 == null && node2 == null && carrier == 0) ){
        val num1 = node1?.`val` ?: 0
        val num2 = node2?.`val` ?: 0
        val sum = num1 + num2 + carrier
        carrier = sum / 10
        val newNode = ListNode(sum % 10)
        curNode.next = newNode
        curNode = newNode
        node1 = node1?.next
        node2 = node2?.next
    }
    return tempHead.next
}

//11. Container With Most Water
fun maxArea(height: IntArray): Int {
    var i = 0
    var j = height.size - 1
    var result = 0
    while ( i <= j ){
        val width = j - i
        val minHeight = Math.min(height[i], height[j])
        result = Math.max(width*minHeight, result)
        if ( height[i] < height[j] ){
            i++
        }else if ( height[i] > height[j]){
            j--
        }else{
            i++
            j--
        }
    }
    return result
}

//1046. Last Stone Weight
fun lastStoneWeight(stones: IntArray): Int {
    val stonesPQ = PriorityQueue<Int>(Collections.reverseOrder())
    stones.forEach {
        stonesPQ.offer(it)
    }
    while ( stonesPQ.size > 1 ){
        val s1 = stonesPQ.poll()
        val s2 = stonesPQ.poll()
        val nS = s1 - s2
        if ( nS > 0 )
            stonesPQ.offer(nS)
    }
    return if ( stonesPQ.isEmpty() ) 0 else stonesPQ.poll()
}

//1260. Shift 2D Grid
fun shiftGrid(grid: Array<IntArray>, k: Int): List<List<Int>> {
    val height = grid.size
    val width = grid[0].size
    val resultGrid = List(height){ List(width){0}.toMutableList() }
    for ( i in 0 until height){
        for ( j in 0 until width ){
            val oriPos = i * width + j
            val newPos = oriPos + k
            val newJ = newPos % width
            val newI = newPos / width % height
            resultGrid[newI][newJ] = grid[i][j]
        }
    }
    return resultGrid
}

//289. Game of Life
fun gameOfLife(board: Array<IntArray>): Unit {
    val directionList = arrayOf(
        intArrayOf(-1,-1), intArrayOf(-1,0), intArrayOf(-1,1),
        intArrayOf(0,-1), intArrayOf(0,1),
        intArrayOf(1,-1), intArrayOf(1,0), intArrayOf(1,1)
    )
    val height = board.size
    val width = board[0].size
    val isLive = 1
    val liveToDie = 2
    val dieToLive = 3
    for ( i in board.indices ){
        for ( j in board[i].indices ){
            val curStatus = board[i][j]
            var nearLive = 0
            for ( direction in directionList ){
                val nearI = i + direction[0]
                val nearJ = j + direction[1]
                if ( (nearI in 0 until height) && (nearJ in 0 until width) && (board[nearI][nearJ] == isLive || board[nearI][nearJ] == liveToDie)  ){
                    nearLive++
                }
            }
            if ( (curStatus == isLive && nearLive < 2) || (curStatus == isLive && nearLive > 3)){
                board[i][j] = liveToDie
            }else if ( curStatus != isLive && nearLive == 3 ) {
                board[i][j] = dieToLive
            }
        }
    }
    for ( i in board.indices ){
        for ( j in board[i].indices){
            if ( board[i][j] == liveToDie ){
                board[i][j] = 0
            }else if ( board[i][j] == dieToLive ){
                board[i][j] = 1
            }
        }
    }
}

//703. Kth Largest Element in a Stream
class KthLargest(k: Int, nums: IntArray) {

    private val pq = PriorityQueue<Int>()
    private val maxK : Int

    init {
        maxK = k
        for ( num in nums ){
            add(num)
        }
    }

    fun add(`val`: Int): Int {
        pq.offer(`val`)
        if ( pq.size > maxK )
            pq.poll()
        return pq.peek()
    }

}

//1518. Water Bottles
fun numWaterBottles(numBottles: Int, numExchange: Int): Int {
    var drinkBottle = 0
    var newBottle = numBottles
    var emptyBottle = 0
    while ( newBottle > 0 ){
        drinkBottle += newBottle
        emptyBottle += newBottle
        newBottle = emptyBottle / numExchange
        emptyBottle %= numExchange
    }
    return drinkBottle
}

//59. Spiral Matrix II
fun generateMatrix(n: Int): Array<IntArray> {
    val result = Array(n){ IntArray(n){0} }
    var curVal = 1
    val directionList = arrayOf(intArrayOf(0,1), intArrayOf(1,0), intArrayOf(0,-1), intArrayOf(-1,0))
    var directionNum = 0
    var i = 0
    var j = 0
    while ( curVal <= n*n ){
        result[i][j] = curVal++
        val curDirection = directionList[directionNum%4]
        val newI = curDirection[0] + i
        val newJ = curDirection[1] + j
        if ( newI in 0 until n && newJ in 0 until n && result[newI][newJ] == 0 ){
            i = newI
            j = newJ
        }else{//change direction if out of bound OR non-zero element
            val newDirection = directionList[++directionNum%4]
            i += newDirection[0]
            j += newDirection[1]
        }
    }
    return result
}

fun main(args: Array<String>) {
    martSixRandomGenerator()
}

fun martSixRandomGenerator(){
    val sixNum = mutableListOf<Int>()
    val excludeList = mutableSetOf(7,14,16,25,36,40)
    while ( sixNum.size < 6 ){
        val temp = (1..49).random()
        if ( !excludeList.contains(temp) ){
            sixNum.add(temp)
            excludeList.add(temp)
        }
    }
    println("result: ${sixNum.toIntArray().sorted()}")
}

fun IntArray.toLinkList(): ListNode?{
    val tempHead = ListNode(-1)
    var curNode = tempHead
    for ( element in this ){
        val newNode = ListNode(element)
        curNode.next = newNode
        curNode = newNode
    }
    return  tempHead.next
}
fun ListNode?.toList(): List<Int>{
    if ( this == null ) return emptyList()
    val tempList = mutableListOf<Int>()
    var curNode = this
    while ( curNode != null ){
        tempList.add(curNode!!.`val`)
        curNode = curNode!!.next
    }
    return tempList
}