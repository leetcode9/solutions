REM    **********
PROMPT TABLE
REM    **********

CREATE TABLE LA WOR

REM    **********
PROMPT TABLE
REM    **********


REM    **********
PROMPT Base Data
REM    **********

INSERT DATA LA WOR


REM    **********
PROMPT Base Data
REM    **********

REM    **********
PROMPT INDEX
REM    **********

CREATE INDEX LA WOR

REM    **********
PROMPT INDEX
REM    **********


REM    **********
PROMPT PRIMARY KEY
REM    **********

CREATE PRIMARY KEY LA WOR

REM    **********
PROMPT PRIMARY KEY
REM    **********


REM    **********
PROMPT SEQUENCE
REM    **********

CREATE SEQUENCE LA WOR

REM    **********
PROMPT SEQUENCE
REM    **********

REM    **********
PROMPT TYPE
REM    **********

CREATE TYPE LA WOR


REM    **********
PROMPT TYPE
REM    **********

REM    **********
PROMPT ROLE
REM    **********

CREATE ROLE LA WOR

REM    **********
PROMPT ROLE
REM    **********

REM    **********
PROMPT SYNONYM
REM    **********

CREATE SYNONYM LA WOR

REM    **********
PROMPT SYNONYM
REM    **********

REM    **********
PROMPT ACCESS RIGHT
REM    **********

CREATE ACCESS RIGHT LA WOR

REM    **********
PROMPT ACCESS RIGHT
REM    **********



