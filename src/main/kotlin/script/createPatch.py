import sys
from os import listdir
from os.path import isfile, isdir, join
from shutil import copyfile
from shutil import move

def main():
     versionStr = sys.argv[1]

     #Generate pchxxxx.sql
     f= open("pch"+versionStr+".sql","w+")    

     f.write('SET DEFINE OFF;'+'\n')
     f.write('SPOOL pch'+versionStr+'.lis'+'\n\n')

     f.write('PROMPT Stopping B2B-Console...'+'\n')
     f.write('execute bpk_console.bsp_end;'+'\n\n')


     f.write('PROMPT Applying table changes'+'\n')
     #indf= open("pch"+versionStr+"tab.sql","w+")
     indf=None
     # Copy sql statement from devpatch from developer.
     sessionHeader = "PROMPT TABLE"
     devPatch = open('devpatch.sql', 'r')     
     Lines = devPatch.readlines()

     copyStr=False
     hasData=False
     # Strips the newline character
     for line in Lines:
         if copyStr:
              if sessionHeader not in line and line.startswith('REM') == False:
                   if (line.strip()):
                        if (indf is None):
                             indf= open("pch"+versionStr+"tab.sql","w+")
                        indf.write(line)
                        hasData=True
         if sessionHeader in line:
              if copyStr == True:
                   copyStr= False
              else:     
                    copyStr= True                    

     devPatch.close()
     # Copy sql statement from devpatch from developer.
     if (indf is not None):
          indf.close()
     if hasData:
          f.write('@@pch'+versionStr+'tab.sql'+'\n')
     
     f.write('PROMPT Applying type changes'+'\n')
     #indf= open("pch"+versionStr+"typ.sql","w+")
     indf=None
     # Copy sql statement from devpatch from developer.
     sessionHeader = "PROMPT TYPE"
     devPatch = open('devpatch.sql', 'r')     
     Lines = devPatch.readlines()

     copyStr=False
     hasData=False
     # Strips the newline character
     for line in Lines:
         if copyStr:
              if sessionHeader not in line and line.startswith('REM') == False:
                   if (line.strip()):
                        if (indf is None):
                             indf= open("pch"+versionStr+"typ.sql","w+")
                        indf.write(line)
                        hasData=True
         if sessionHeader in line:
              if copyStr == True:
                   copyStr= False
              else:     
                    copyStr= True                    

     devPatch.close()
     # Copy sql statement from devpatch from developer.
     if (indf is not None):
          indf.close()
     if hasData:
          f.write('@@pch'+versionStr+'typ.sql'+'\n\n')
     

     f.write('PROMPT Applying view changes'+'\n\n')
     #lookup files under directory ./GMS/prc
     packageDir = "./GMS/vw"
     destDir="."
     try:
          packageFiles = listdir(packageDir)

          # Read Files 
          for packageFile in packageFiles:
          #
              fullpath = join(packageDir, packageFile)

              destpath = join(destDir, packageFile)         
          # 
              if isfile(fullpath):
                 # copy file to current directory
                 copyfile(fullpath, destpath)
                 f.write('PROMPT '+packageFile+'\n')
                 f.write('@@'+packageFile+'\n\n')
     except OSError as e:
          print('No view change found')       

     f.write('PROMPT Applying new indexes'+'\n')

     #Generate pchxxxxind.sql
     #indf= open("pch"+versionStr+"ind.sql","w+")
     indf=None
     # Copy sql statement from devpatch from developer.
     sessionHeader = "PROMPT INDEX"
     devPatch = open('devpatch.sql', 'r')     
     Lines = devPatch.readlines()

     copyStr=False
     hasData=False
     # Strips the newline character
     for line in Lines:
         if copyStr:
              if sessionHeader not in line and line.startswith('REM') == False:
                   if (line.strip()):
                        if (indf is None):
                             indf= open("pch"+versionStr+"ind.sql","w+")
                        indf.write(line)
                        hasData=True
         if sessionHeader in line:
              if copyStr == True:
                   copyStr= False
              else:     
                    copyStr= True                    

     devPatch.close()
     # Copy sql statement from devpatch from developer.
     if (indf is not None):
          indf.close()
     if hasData:
          f.write('@@pch'+versionStr+'ind.sql'+'\n\n')

     f.write('PROMPT Applying new primary keys'+'\n\n')
     #Generate pchxxxxpky.sql
     #indf= open("pch"+versionStr+"pky.sql","w+")
     indf=None
     # Copy sql statement from devpatch from developer.
     sessionHeader = "PROMPT PRIMARY KEY"
     devPatch = open('devpatch.sql', 'r')     
     Lines = devPatch.readlines()

     copyStr=False
     hasData=False
     # Strips the newline character
     for line in Lines:
         if copyStr:
              if sessionHeader not in line and line.startswith('REM') == False:
                   if (line.strip()):
                        if (indf is None):
                             indf= open("pch"+versionStr+"pky.sql","w+")
                        indf.write(line)
                        hasData=True
         if sessionHeader in line:
              if copyStr == True:
                   copyStr= False
              else:     
                    copyStr= True                    

     devPatch.close()
     # Copy sql statement from devpatch from developer.
     if (indf is not None):
          indf.close()
     if hasData:
          f.write('@@pch'+versionStr+'pky.sql'+'\n\n')
     

     f.write('PROMPT Applying new sequences'+'\n\n')     
     #Generate pchxxxxseq.sql
     #indf= open("pch"+versionStr+"seq.sql","w+")
     indf=None
     # Copy sql statement from devpatch from developer.
     sessionHeader = "PROMPT SEQUENCE"
     devPatch = open('devpatch.sql', 'r')     
     Lines = devPatch.readlines()

     copyStr=False
     hasData=False
     # Strips the newline character
     for line in Lines:
         if copyStr:
              if sessionHeader not in line and line.startswith('REM') == False:
                   if (line.strip()):
                        if (indf is None):
                             indf= open("pch"+versionStr+"seq.sql","w+")
                        indf.write(line)
                        hasData=True
         if sessionHeader in line:
              if copyStr == True:
                   copyStr= False
              else:     
                    copyStr= True                    

     devPatch.close()
     # Copy sql statement from devpatch from developer.
     if (indf is not None):
          indf.close()
     if hasData:
          f.write('@@pch'+versionStr+'seq.sql'+'\n\n')
     

     f.write('PROMPT Applying triggers'+'\n')
     #lookup files under directory ./GMS/trg
     packageDir = "./GMS/trg"
     destDir="."
     try:
          packageFiles = listdir(packageDir)

          # Read Files 
          for packageFile in packageFiles:
          #
              fullpath = join(packageDir, packageFile)

              destpath = join(destDir, packageFile)         
          # 
              if isfile(fullpath):
                 # copy file to current directory
                 copyfile(fullpath, destpath)
                 f.write('PROMPT '+packageFile+'\n')
                 f.write('@@'+packageFile+'\n\n')
     except OSError as e:
          print('No trigger change found')
     
     f.write('PROMPT Applying functions'+'\n')
     f.write('PROMPT Applying procedures'+'\n')
     f.write('PROMPT Applying packages'+'\n\n')

     #lookup files under directory ./GMS/prc
     # FIND PRC COMMENT START
     # packageDir = "./GMS/prc"
     # destDir="."
     # packageFiles = listdir(packageDir)

     # # Read Files 
     # for packageFile in packageFiles:
     # #
     #     fullpath = join(packageDir, packageFile)

     #     destpath = join(destDir, packageFile)         
     # # 
     #     if isfile(fullpath):
     #        # copy file to current directory
     #        copyfile(fullpath, destpath)
     #        f.write('PROMPT '+packageFile+'\n')
     #        f.write('@@'+packageFile+'\n\n')
     # FIND PRC COMMENT 
     #elif isdir(fullpath):
     #  print("Dir：", f)
     #End lookup files under directory ./GMS/prc
     
     f.write('PROMPT Applying new grants to gms_role \n')     
     #Generate pchxxxxrol.sql
     #indf= open("pch"+versionStr+"seq.sql","w+")
     indf=None
     # Copy sql statement from devpatch from developer.
     sessionHeader = "PROMPT ROLE"
     devPatch = open('devpatch.sql', 'r')     
     Lines = devPatch.readlines()

     copyStr=False
     hasData=False
     # Strips the newline character
     for line in Lines:
         if copyStr:
              if sessionHeader not in line and line.startswith('REM') == False:
                   if (line.strip()):
                        if (indf is None):
                             indf= open("pch"+versionStr+"rol.sql","w+")
                        indf.write(line)
                        hasData=True
         if sessionHeader in line:
              if copyStr == True:
                   copyStr= False
              else:     
                    copyStr= True                    

     devPatch.close()
     # Copy sql statement from devpatch from developer.
     if (indf is not None):
          indf.close()
     if hasData:
          f.write('@@pch'+versionStr+'rol.sql \n\n')
     

     f.write('PROMPT Applying new synonyms \n')
     #Generate pchxxxxsyn.sql
     #indf= open("pch"+versionStr+"syn.sql","w+")
     indf=None
     # Copy sql statement from devpatch from developer.
     sessionHeader = "PROMPT SYNONYM"
     devPatch = open('devpatch.sql', 'r')     
     Lines = devPatch.readlines()

     copyStr=False
     hasData=False
     # Strips the newline character
     for line in Lines:
         if copyStr:
              if sessionHeader not in line and line.startswith('REM') == False:
                   if (line.strip()):
                        if (indf is None):
                             indf= open("pch"+versionStr+"syn.sql","w+")
                        indf.write(line)
                        hasData=True
         if sessionHeader in line:
              if copyStr == True:
                   copyStr= False
              else:     
                    copyStr= True                    

     devPatch.close()
     # Copy sql statement from devpatch from developer.
     if (indf is not None):
          indf.close()
     if hasData:
          f.write('@@pch'+versionStr+'syn.sql \n\n')
     
     f.write('PROMPT Applying Production Fix Data'+'\n')
     #lookup files under directory ./ProdFix
     packageDir = "./ProdFix"
     destDir="./ProdFix/bak"
     destDir2="."
     try:
          packageFiles = listdir(packageDir)

          # Read Files 
          for packageFile in packageFiles:
          #
              fullpath = join(packageDir, packageFile)

              destpath = join(destDir, packageFile)
              destpath2 = join(destDir2, packageFile)         
          # 
              if isfile(fullpath):
                 # copy file to current directory
                 #copyfile(fullpath, destpath)
                 copyfile(fullpath, destpath2)
                 move(fullpath, destpath)
                 f.write('PROMPT '+packageFile+'\n')
                 f.write('@@'+packageFile+'\n\n')
     except OSError as e:
          print('No trigger change found')

     f.write('PROMPT Applying base data \n')
     #Generate pchxxxxdat.sql
     
     #indf= open("pch"+versionStr+"dat.sql","w+")
     indf= open("pch"+versionStr+"dat.sql","w+")
     indf.write('INSERT INTO APPL_MOD_OPTS\n')
     indf.write(' (AM_AM_TYPE, AM_AM_ID, BUTTON_ID, LABEL, DESCR)\n')
     indf.write('SELECT AM_TYPE, AM_ID,\n')
     indf.write("'FILE.EXCEL','Copy To Excel',\n")
     indf.write("'Copy records to an Excel spreadsheet'\n")
     indf.write('FROM   APPL_MODS am\n')
     indf.write("WHERE NOT EXISTS (SELECT 'X'\n")
     indf.write('FROM  APPL_MOD_OPTS amo\n')
     indf.write('WHERE amo.am_am_id = am.am_id)\n')
     indf.write("AND AM_TYPE = '01';\n\n\n")

     indf.write('REM    **********\n')
     indf.write('PROMPT BASE DATA\n')
     indf.write('REM    **********\n\n')
     # Copy sql statement from devpatch from developer.
     sessionHeader = "PROMPT Base Data"
     devPatch = open('devpatch.sql', 'r')     
     Lines = devPatch.readlines()

     copyStr=False
     hasData=False
     # Strips the newline character
     for line in Lines:
         if copyStr:
              if sessionHeader not in line and line.startswith('REM') == False:
                   if (line.strip()):
                        if (indf is None):
                             indf= open("pch"+versionStr+"dat.sql","w+")
                        indf.write(line)
                        hasData=True
         if sessionHeader in line:
              if copyStr == True:
                   copyStr= False
              else:     
                    copyStr= True                    

     devPatch.close()

     indf.write('REM\n')
     indf.write('REM Below statements should be put at the end of this script\n')
     indf.write('REM\n')

     indf.write('INSERT INTO USR_OPT_PLATFORM_MAPS\n')
     indf.write('( USO_ART_USO_ID, PLATFORM )\n')
     indf.write("SELECT USO.ART_USO_ID, '2'\n")
     indf.write('FROM   USR_SETTING_OPTS USO\n')
     indf.write("WHERE  NOT EXISTS (SELECT 1 FROM USR_OPT_PLATFORM_MAPS UOPM WHERE UOPM.USO_ART_USO_ID = USO.ART_USO_ID AND UOPM.PLATFORM = '2')\n")
     indf.write("AND    USO.FIELD_NAME <> 'DCN_HDR_ID_NO_LNK';\n\n")

     indf.write('INSERT INTO USR_OPT_PLATFORM_MAPS\n')
     indf.write('  ( USO_ART_USO_ID, PLATFORM )\n')
     indf.write("SELECT USO.ART_USO_ID, '3'\n")
     indf.write('FROM   USR_SETTING_OPTS USO\n')
     indf.write("WHERE  NOT EXISTS (SELECT 1 FROM USR_OPT_PLATFORM_MAPS UOPM WHERE UOPM.USO_ART_USO_ID = USO.ART_USO_ID AND UOPM.PLATFORM = '3')\n")
     indf.write("AND    USO.FIELD_NAME <> 'DCN_HDR_ID_NO_LNK';\n\n")

     indf.write('INSERT INTO USR_OPT_PLATFORM_MAPS\n')
     indf.write('  ( USO_ART_USO_ID, PLATFORM )\n')
     indf.write("SELECT USO.ART_USO_ID, '9'\n")
     indf.write('FROM   USR_SETTING_OPTS USO\n')
     indf.write("WHERE  USO.AM_AM_ID NOT LIKE 'JMEN%'\n")
     indf.write("AND    NOT EXISTS (SELECT 1 FROM USR_OPT_PLATFORM_MAPS UOPM WHERE UOPM.USO_ART_USO_ID = USO.ART_USO_ID AND UOPM.PLATFORM = '9');\n")

     #Must has data
     hasData=True
     
     #f.close()  
     # Copy sql statement from devpatch from developer.
     if (indf is not None):
          indf.close()
     if hasData:
          f.write('@@pch'+versionStr+'dat.sql \n\n')  

     
     f.write('PROMPT Inserting into GMS_PATCHES \n')
     f.write('@@pch'+versionStr+'pch_upd.sql \n\n')

     f.write('COMMIT \n')
     f.write('/ \n')
     f.write('spool off \n\n')

     f.write('@@recomp \n')
     f.write('@@recomp \n')
     f.write('@@recomp \n')
     f.write('@@recomp \n')
     f.write('@@recomp \n\n')
     
     f.close()   

     #Generate pchxxxxpch_upd.sql
     #
     versionNumber = versionStr[0:1] + "." +versionStr[1:3] +"."+ versionStr[-3:]
     f= open("pch"+sys.argv[1]+"pch_upd.sql","w+")

     f.write('INSERT INTO GMS_PATCHES\n')
     f.write('(GP_ID, GV_GMS_ID, DATE_INSTD, DB_CHGS_INCD)\n')
     f.write('VALUES\n')
     f.write("('"+versionNumber+"','5.00',SYSDATE,'1');\n")

     f.close()
     
if __name__== "__main__":
  main()
