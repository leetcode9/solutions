import java.util.*
import kotlin.test.*

internal class SolutionsKtTest{

    @Test
    fun testGenerateMatrix() {
        val questionNumber = 59

        val expect1 = arrayOf(intArrayOf(1,2,3), intArrayOf(8,9,4), intArrayOf(7,6,5))
        val result1 = generateMatrix(3)
        assertTrue(same2DArray(expect1, result1))

        val expect2 = arrayOf(intArrayOf(1))
        val result2 = generateMatrix(1)
        assertTrue(same2DArray(expect2, result2))
    }

    @Test
    fun testNumWaterBottles() {
        val questionNumber = 1518

        val expect1 = numWaterBottles(9,3)
        val result1 = 13
        assertEquals(expect1, result1, "$questionNumber test 1 fail")

        val expect2 = numWaterBottles(15,4)
        val result2 = 19
        assertEquals(expect2, result2, "$questionNumber test 1 fail")
    }

    @Test
    fun testKthLargest(){
        val questionNumber = 703

        val kthLargest = KthLargest(3, intArrayOf(4,5,8,2))
        assertEquals(4, kthLargest.add(3) )   // return 4
        assertEquals(5,kthLargest.add(5)) ;   // return 5
        assertEquals(5, kthLargest.add(10)) ;  // return 5
        assertEquals(8,kthLargest.add(9)) ;   // return 8
        assertEquals(8, kthLargest.add(4)) ;   // return 8

    }

    private fun same2DArray(expect: Array<IntArray>, result: Array<IntArray>): Boolean{
        if ( expect.size != result.size ) return false
        for ( i in expect.indices ){
            if ( expect[i].size != result[i].size ) return false
            for ( j in expect[i].indices ){
                if ( expect[i][j] != result[i][j] ) return false
            }
        }
        return true
    }

    @Test
    fun testGameOfLife() {
        val questionNumber = 289

        val board1 = arrayOf(intArrayOf(0,1,0), intArrayOf(0,0,1), intArrayOf(1,1,1), intArrayOf(0,0,0))
        val expect1 = arrayOf(intArrayOf(0,0,0), intArrayOf(1,0,1), intArrayOf(0,1,1), intArrayOf(0,1,0))
        val result1 = gameOfLife(board1)
        assertTrue(same2DArray(expect1, board1))

        val board2 = arrayOf(intArrayOf(1,1), intArrayOf(1,0))
        val expect2 = arrayOf(intArrayOf(1,1), intArrayOf(1,1))
        val result2 = gameOfLife(board2)
        assertTrue(same2DArray(expect2, board2))
    }

    @Test
    fun testShiftGrid() {
        val questionNumber = 1260

        val grid1 = arrayOf(intArrayOf(1,2,3), intArrayOf(4,5,6), intArrayOf(7,8,9))
        val expect1 = arrayOf(intArrayOf(9,1,2), intArrayOf(3,4,5), intArrayOf(6,7,8)).map { it.toList() }.flatten()
        val result1 = shiftGrid(grid1, 1).map { it.toList() }.flatten()
        assertTrue(expect1.size == result1.size && expect1.containsAll(result1) && result1.containsAll(expect1));

        val grid2 = arrayOf(intArrayOf(3,8,1,9), intArrayOf(19,7,2,5), intArrayOf(4,6,11,10), intArrayOf(12,0,21,13))
        val expect2 = arrayOf(intArrayOf(12,0,21,13), intArrayOf(3,8,1,9), intArrayOf(19,7,2,5), intArrayOf(4,6,11,10)).map { it.toList() }.flatten()
        val result2 = shiftGrid(grid2, 4).map { it.toList() }.flatten()
        assertTrue(expect2.size == result2.size && expect2.containsAll(result2) && result2.containsAll(expect2));

        val grid3 =  arrayOf(intArrayOf(1,2,3), intArrayOf(4,5,6), intArrayOf(7,8,9))
        val expect3 = arrayOf(intArrayOf(9,1,2), intArrayOf(3,4,5), intArrayOf(6,7,8)).map { it.toList() }.flatten()
        val result3 = shiftGrid(grid3, 1).map { it.toList() }.flatten()
        assertTrue(expect3.size == result3.size && expect3.containsAll(result3) && result3.containsAll(expect3));

    }

    @Test
    fun testLastStoneWeight() {
        val questionNumber = 1046

        val expect1 = 1
        val result1 = lastStoneWeight(intArrayOf(2,7,4,1,8,1))
        assertEquals(expect1, result1, "$questionNumber test 1 fail")

        val expect2 = 1
        val result2 = lastStoneWeight(intArrayOf(1))
        assertEquals(expect2, result2, "$questionNumber test 2 fail")

        val expect3 = 0
        val result3 = lastStoneWeight(intArrayOf(2,2))
        assertEquals(expect3, result3, "$questionNumber test 2 fail")
    }

    @Test
    fun testMaxArea() {
        val questionNumber = 11

        val expect1 = 49
        val result1 = maxArea(intArrayOf( 1,8,6,2,5,4,8,3,7))
        assertEquals(expect1, result1, "$questionNumber test 1 fail")

        val expect2 = 1
        val result2 = maxArea(intArrayOf(1,1))
        assertEquals(expect2, result2, "$questionNumber test 2 fail")

    }

    //410. Split Array Largest Sum
    fun splitArray(nums: IntArray, m: Int): Int {
        val compareIntArray = Comparator<List<Int>> { o1, o2 ->
            val result = if (o1.sum() == o2.sum()) {
                if (o2.size == o1.size){
                    var diff = 0
                    for ( i in o1.indices ){
                        if ( o2[i] != o1[i] ){
                            diff = o2[i] - o1[i]
                            break
                        }
                    }
                    diff
                }else{
                    o2.size - o1.size
                }
            } else {
                o2.sum() - o1.sum()
            }
            result
        }
        val singleArray = mutableListOf<List<Int>>()
        val subArrayPQ = PriorityQueue(compareIntArray)
        subArrayPQ.add(nums.toList())
        while ( subArrayPQ.size + singleArray.size < m ){
            val rightArray = subArrayPQ.poll().toMutableList()
            if ( rightArray.size == 1){
                singleArray.add(rightArray)
                continue
            }
            val leftArray = mutableListOf<Int>()
            var minDiff = rightArray.sum()
            while ( rightArray.size >= 1 ){
                val temp = rightArray[0]
                val nextDiff = Math.abs((leftArray.sum()+temp) - (rightArray.sum()-temp))
                if ( nextDiff > minDiff ){
                    break
                }else{
                    minDiff = nextDiff
                    leftArray.add(rightArray.removeAt(0))
                }
            }
            subArrayPQ.offer(leftArray)
            subArrayPQ.offer(rightArray)
        }
        subArrayPQ.addAll(singleArray)
        return subArrayPQ.peek().sum()
    }

    @Test
    fun splitArray() {
        val questionNumber = 410

        val expect1 = 18
        val result1 = splitArray(intArrayOf(7,2,5,10,8), 2)
        assertEquals(expect1, result1, "$questionNumber test 1 fail")

        val expect2 = 9
        val result2 = splitArray(intArrayOf(1,2,3,4,5), 2)
        assertEquals(expect2, result2, "$questionNumber test 2 fail")

        val expect3 = 4
        val result3 = splitArray(intArrayOf(1,4,4), 3)
        assertEquals(expect3, result3, "$questionNumber test 3 fail")

        val expect5 = 5
        val result5 = splitArray(intArrayOf(1,2,3,4,5), 4)
        assertEquals(expect5, result5, "$questionNumber test 5 fail")

        val expect6 = 5
        val result6 = splitArray(intArrayOf(1,2,3,4,5), 5)
        assertEquals(expect6, result6, "$questionNumber test 6 fail")

        val expect4 = 194890
        val result4 = splitArray(intArrayOf(5334,6299,4199,9663,8945,3566,9509,3124,6026,6250,7475,5420,9201,9501,38,5897,4411,6638,9845,161,9563,8854,3731,5564,5331,4294,3275,1972,1521,2377,3701,6462,6778,187,9778,758,550,7510,6225,8691,3666,4622,9722,8011,7247,575,5431,4777,4032,8682,5888,8047,3562,9462,6501,7855,505,4675,6973,493,1374,3227,1244,7364,2298,3244,8627,5102,6375,8653,1820,3857,7195,7830,4461,7821,5037,2918,4279,2791,1500,9858,6915,5156,970,1471,5296,1688,578,7266,4182,1430,4985,5730,7941,3880,607,8776,1348,2974,1094,6733,5177,4975,5421,8190,8255,9112,8651,2797,335,8677,3754,893,1818,8479,5875,1695,8295,7993,7037,8546,7906,4102,7279,1407,2462,4425,2148,2925,3903,5447,5893,3534,3663,8307,8679,8474,1202,3474,2961,1149,7451,4279,7875,5692,6186,8109,7763,7798,2250,2969,7974,9781,7741,4914,5446,1861,8914,2544,5683,8952,6745,4870,1848,7887,6448,7873,128,3281,794,1965,7036,8094,1211,9450,6981,4244,2418,8610,8681,2402,2904,7712,3252,5029,3004,5526,6965,8866,2764,600,631,9075,2631,3411,2737,2328,652,494,6556,9391,4517,8934,8892,4561,9331,1386,4636,9627,5435,9272,110,413,9706,5470,5008,1706,7045,9648,7505,6968,7509,3120,7869,6776,6434,7994,5441,288,492,1617,3274,7019,5575,6664,6056,7069,1996,9581,3103,9266,2554,7471,4251,4320,4749,649,2617,3018,4332,415,2243,1924,69,5902,3602,2925,6542,345,4657,9034,8977,6799,8397,1187,3678,4921,6518,851,6941,6920,259,4503,2637,7438,3893,5042,8552,6661,5043,9555,9095,4123,142,1446,8047,6234,1199,8848,5656,1910,3430,2843,8043,9156,7838,2332,9634,2410,2958,3431,4270,1420,4227,7712,6648,1607,1575,3741,1493,7770,3018,5398,6215,8601,6244,7551,2587,2254,3607,1147,5184,9173,8680,8610,1597,1763,7914,3441,7006,1318,7044,7267,8206,9684,4814,9748,4497,2239), 9)
        assertEquals(expect4, result4, "$questionNumber test 4 fail")

    }

    //895. Maximum Frequency Stack
    class FreqStack() {

        fun push(element: Int) {

        }

        fun pop(): Int {
            return 0
        }
    }


    @Test
    fun testFreqStack(){
        val questionNumber = 895

        val freqStack = FreqStack()
        freqStack.push(5) // The stack is [5]
        freqStack.push(7) // The stack is [5,7]
        freqStack.push(5) // The stack is [5,7,5]
        freqStack.push(7) // The stack is [5,7,5,7]
        freqStack.push(4) // The stack is [5,7,5,7,4]
        freqStack.push(5) // The stack is [5,7,5,7,4,5]

        val pop1 = freqStack.pop() // return 5, as 5 is the most frequent. The stack becomes [5,7,5,7,4].
        val expectPop1 = 5
        assertEquals(expectPop1, pop1, "$questionNumber Pop 1 fail")

        val pop2 = freqStack.pop() // return 7, as 5 and 7 is the most frequent, but 7 is closest to the top. The stack becomes [5,7,5,4].
        val expectPop2 = 7
        assertEquals(expectPop2, pop2, "$questionNumber Pop 2 fail")

        val pop3 = freqStack.pop() // return 5, as 5 is the most frequent. The stack becomes [5,7,4].
        val expectPop3 = 5
        assertEquals(expectPop3, pop3, "$questionNumber Pop 3 fail")

        val pop4 = freqStack.pop() // return 4, as 4, 5 and 7 is the most frequent, but 4 is closest to the top. The stack becomes [5,7].
        val expectPop4 = 4
        assertEquals(expectPop4, pop4, "$questionNumber Pop 4 fail")

    }

    @Test
    fun testAddTwoNumbers() {
        val questionNumber = 2

        val expect1 = listOf(7,0,8)
        val result1 = addTwoNumbers(intArrayOf(2,4,3).toLinkList(), intArrayOf(5,6,4).toLinkList()).toList()
        assertTrue(expect1.size == result1.size && expect1.containsAll(result1) && result1.containsAll(expect1));


        val expect2 = listOf(0)
        val result2 = addTwoNumbers(intArrayOf(0).toLinkList(), intArrayOf(0).toLinkList()).toList()
        assertTrue(expect2.size == result2.size && expect2.containsAll(result2) && result2.containsAll(expect2));

        val expect3 = listOf(8,9,9,9,0,0,0,1)
        val result3 = addTwoNumbers(intArrayOf(9,9,9,9,9,9,9).toLinkList(), intArrayOf(9,9,9,9).toLinkList()).toList()
        assertTrue(expect3.size == result3.size && expect3.containsAll(result3) && result3.containsAll(expect3));

    }

    @Test
    fun testSwapNodes() {
        val questionNumber = 1721

        val expect1 = listOf(1,4,3,2,5)
        val result1 = swapNodes(intArrayOf(1,2,3,4,5).toLinkList(), 2).toList()
        assertTrue(expect1.size == result1.size && expect1.containsAll(result1) && result1.containsAll(expect1));


        val expect2 = listOf(7,9,6,6,8,7,3,0,9,5)
        val result2 = swapNodes(intArrayOf(7,9,6,6,7,8,3,0,9,5).toLinkList(), 5).toList()
        assertTrue(expect2.size == result2.size && expect2.containsAll(result2) && result2.containsAll(expect2));

    }

    @Test
    fun testsSearchMatrix() {
        val questionNumber = 74

        val matrix1 = arrayOf(
            intArrayOf(1,3,5,7),
            intArrayOf(10,11,16,20),
            intArrayOf(23,30,34,60)
        )
        val expect1 = true
        val result1 = searchMatrix(matrix1, 3)
        assertEquals(expect1, result1, "$questionNumber test 1 fail")

        val matrix2 = arrayOf(
            intArrayOf(1,3,5,7),
            intArrayOf(10,11,16,20),
            intArrayOf(23,30,34,60)
        )
        val expect2 = false
        val result2 = searchMatrix(matrix2, 13)
        assertEquals(expect2, result2, "$questionNumber test 2 fail")

    }

    @Test
    fun testTwoCitySchedCost() {
        val questionNumber = 1029

        val expect1 = 110
        val result1 = twoCitySchedCost(arrayOf(intArrayOf(10,20), intArrayOf(30,200), intArrayOf(400,50), intArrayOf(30,20)))
        assertEquals(expect1, result1, "$questionNumber test 1 fail")

        val expect2 = 1859
        val result2 = twoCitySchedCost(arrayOf(intArrayOf(259,770), intArrayOf(448,54),
            intArrayOf(926,667), intArrayOf(184,139), intArrayOf(840,118), intArrayOf(577,469)))
        assertEquals(expect2, result2, "$questionNumber test 2 fail")

        val expect3 = 3086
        val result3 = twoCitySchedCost(arrayOf(intArrayOf(515,563), intArrayOf(451,713), intArrayOf(537,709),
            intArrayOf(343,819), intArrayOf(855,779), intArrayOf(457,60), intArrayOf(650,359), intArrayOf(631,42)))
        assertEquals(expect3, result3, "$questionNumber test 3 fail")

    }

    @Test
    fun testFindDuplicate(){
        val questionNumber = 287

        val expect1 = 2
        val result1 = findDuplicate(intArrayOf(1,3,4,2,2))
        assertEquals(expect1, result1, "$questionNumber test 1 fail")

        val expect2 = 3
        val result2 = findDuplicate(intArrayOf(3,1,3,4,2))
        assertEquals(expect2, result2, "$questionNumber test 2 fail")


    }


    @Test
    fun testLetterCombinations() {
        val questionNumber = 17

        val expect1 = listOf("ad","ae","af","bd","be","bf","cd","ce","cf")
        val result1 = letterCombinations("23")
        assertTrue(expect1.size == result1.size && expect1.containsAll(result1) && result1.containsAll(expect1));

        val expect2 = emptyList<String>()
        val result2 = letterCombinations("")
        assertTrue(expect2.size == result2.size && expect2.containsAll(result2) && result2.containsAll(expect2));

        val expect3 = listOf("a","b","c")
        val result3 = letterCombinations("2")
        assertTrue(expect3.size == result3.size && expect3.containsAll(result3) && result3.containsAll(expect3));

    }


    @Test
    fun testReversePrefix() {
        val questionNumber = 2000

        val expect1 = "dcbaefd"
        val result1 = reversePrefix("abcdefd", 'd')
        assertEquals(expect1, result1, "$questionNumber test 1 fail")

        val expect2 = "zxyxxe"
        val result2 = reversePrefix("xyxzxe", 'z')
        assertEquals(expect2, result2, "$questionNumber test 2 fail")

        val expect3 = "abcd"
        val result3 = reversePrefix("abcd", 'z')
        assertEquals(expect3, result3, "$questionNumber test 3 fail")

    }

    @Test
    fun testNumRescueBoats() {
        val questionNumber = 881

        val expect1 = 1
        val result1 = numRescueBoats(intArrayOf(1, 2), 3)
        assertEquals(expect1, result1, "$questionNumber test 1 fail")

        val expect2 = 3
        val result2 = numRescueBoats(intArrayOf(3, 2, 2, 1), 3)
        assertEquals(expect2, result2, "$questionNumber test 2 fail")

        val expect3 =4
        val result3 = numRescueBoats(intArrayOf(3, 5, 3, 4), 5)
        assertEquals(expect3, result3, "$questionNumber test 3 fail")

        val expect4 =3
        val result4 = numRescueBoats(intArrayOf(3,2,3,2,2), 6)
        assertEquals(expect4, result4, "$questionNumber test 3 fail")

    }

    @Test
    fun testBrokenCalc() {
        val questionNumber = 991

        val expect1 = 2
        val result1 = brokenCalc(2, 3)
        assertEquals(expect1, result1, "$questionNumber test 1 fail")

        val expect2 = 2
        val result2 = brokenCalc(5, 8)
        assertEquals(expect2, result2, "$questionNumber test 2 fail")

        val expect3 = 3
        val result3 = brokenCalc(3, 10)
        assertEquals(expect3, result3, "$questionNumber test 3 fail")

    }

    @Test
    fun testGetSmallestString() {
        val questionNumber = 1663

        val expect1 = "aay"
        val result1 = getSmallestString( n = 3, k = 27)
        assertEquals(expect1, result1, "$questionNumber test 1 fail")

        val expect2 = "aaszz"
        val result2 = getSmallestString( n = 5, k = 73)
        assertEquals(expect2, result2, "$questionNumber test 2 fail")

        val expect3 = "aa"
        val result3 = getSmallestString( n = 2, k = 2)
        assertEquals(expect3, result3, "$questionNumber test 3 fail")

        val expect4 = "aaabz"
        val result4 = getSmallestString( n = 5, k = 31)
        assertEquals(expect4, result4, "$questionNumber test 3 fail")


    }

    @Test
    fun partitionLabels() {
        val questionNumber = 763

        val expect1 = listOf(9,7,8)
        val result1 = partitionLabels("ababcbacadefegdehijhklij")
        assertContentEquals(expect1, result1, "$questionNumber test 1 fail")

        val expect2 = listOf(10)
        val result2 = partitionLabels("eccbbbbdec")
        assertContentEquals(expect2, result2, "$questionNumber test 2 fail")

        val expect3 = listOf(1)
        val result3 = partitionLabels("a")
        assertContentEquals(expect3, result3, "$questionNumber test 3 fail")

        val expect4 = listOf(1,1)
        val result4 = partitionLabels("ab")
        assertContentEquals(expect4, result4, "$questionNumber test 4 fail")

    }

    @Test
    fun testMinDominoRotations() {
        val questionNumber = 1007

        val expect1 = 2
        val result1 = minDominoRotations(intArrayOf(2,1,2,4,2,2), intArrayOf(5,2,6,2,3,2))
        assertEquals(expect1, result1, "$questionNumber test 1 fail")

        val expect2 = -1
        val result2 = minDominoRotations(intArrayOf(3,5,1,2,3), intArrayOf(3,6,3,3,4))
        assertEquals(expect2, result2, "$questionNumber test 2 fail")

        val expect3 = -1
        val result3 = minDominoRotations(intArrayOf(2,3,2,1,1,1,2,2), intArrayOf(2,1,2,1,1,3,1,1))
        assertEquals(expect3, result3, "$questionNumber test 3 fail")

    }


    @Test
    fun findLengthOfLCIS() {
        val questionNumber = 674

        val expect1 = 3
        val result1 = findLengthOfLCIS(intArrayOf(1,3,5,4,7))
        assertEquals(expect1, result1, "$questionNumber test 1 fail")

        val expect2 = 1
        val result2 = findLengthOfLCIS(intArrayOf(2,2,2,2,2))
        assertEquals(expect2, result2, "$questionNumber test 2 fail")

    }

    @Test
    fun testRemoveDuplicateLetters(){
        val questionNumber = 316

        val expect1 = "abc"
        val result1 = removeDuplicateLetters("bcabc")
        assertEquals(expect1, result1, "$questionNumber test 1 fail")

        val expect2 = "acdb"
        val result2 = removeDuplicateLetters("cbacdcbc")
        assertEquals(expect2, result2, "$questionNumber test 2 fail")

        val expect3 = "abcd"
        val result3 = removeDuplicateLetters("cbacdcbcd")
        assertEquals(expect3, result3, "$questionNumber test 2 fail")
    }

    @Test
    fun testReverse() {
        val questionNumber = 7

        val expect1 = 321
        val result1 = reverse(123)
        assertEquals(expect1, result1, "$questionNumber test 1 fail")

        val expect2 = -321
        val result2 = reverse(-123)
        assertEquals(expect2, result2, "$questionNumber test 2 fail")

        val expect3 = 21
        val result3 = reverse(120)
        assertEquals(expect3, result3, "$questionNumber test 2 fail")
    }

    @Test
    fun testValidateStackSequences() {
        val questionNumber = 946

        val expect1 = true
        val result1 = validateStackSequences(pushed = intArrayOf(1,2,3,4,5), popped = intArrayOf(4,5,3,2,1))
        assertEquals(expect1, result1, "$questionNumber test 1 fail")

        val expect2 = false
        val result2 = validateStackSequences(pushed = intArrayOf(1,2,3,4,5), popped = intArrayOf(4,3,5,1,2))
        assertEquals(expect2, result2, "$questionNumber test 2 fail")

        val expect3 = true
        val result3 = validateStackSequences(pushed = intArrayOf(1,0), popped = intArrayOf(1,0))
        assertEquals(expect3, result3, "$questionNumber test 2 fail")

    }

    @Test
    fun testFinalValueAfterOperations() {
        val questionNumber = 2011

        val expect1 = 1
        val result1 = finalValueAfterOperations(arrayOf("--X","X++","X++"))
        assertEquals(expect1, result1, "$questionNumber test 1 fail")

        val expect2 = 3
        val result2 = finalValueAfterOperations(arrayOf("++X","++X","X++"))
        assertEquals(expect2, result2, "$questionNumber test 2 fail")

        val expect3 = 0
        val result3 = finalValueAfterOperations(arrayOf("X++","++X","--X","X--"))
        assertEquals(expect3, result3, "$questionNumber test 3 fail")
    }

    @Test
    fun testMinCostClimbingStairs() {
        val questionNumber = 746

        val expect1 = 15
        val result1 = minCostClimbingStairs(intArrayOf(10,15,20))
        assertEquals(expect1, result1, "$questionNumber test 1 fail")

        val expect2 = 6
        val result2 = minCostClimbingStairs(intArrayOf(1,100,1,1,1,100,1,1,100,1))
        assertEquals(expect2, result2, "$questionNumber test 2 fail")
    }


    @Test
    fun testSortArrayByParityII(){
        val questionNumber = 922

        val expect1 = intArrayOf(4,5,2,7)
        val result1 = sortArrayByParityII(intArrayOf(4,2,5,7))
        assertContentEquals(expect1, result1, "$questionNumber test 1 fail")

        val expect2 = intArrayOf(2,3)
        val result2 = sortArrayByParityII(intArrayOf(2,3))
        assertContentEquals(expect2, result2, "$questionNumber test 2 fail")

    }

    @Test
    fun testGetRow() {
        val questionNumber = 119

        val expect1 = listOf(1,3,3,1)
        val result1 = getRow(3)
        assertContentEquals(expect1, result1, "$questionNumber test 1 fail")

        val expect2 = listOf(1)
        val result2 = getRow(0)
        assertContentEquals(expect2, result2, "$questionNumber test 2 fail")

        val expect3 = listOf(1,1)
        val result3 = getRow(1)
        assertContentEquals(expect3, result3, "$questionNumber test 3 fail")

        val expect4 = listOf(1,4,6,4,1)
        val result4 = getRow(4)
        assertContentEquals(expect4, result4, "$questionNumber test 3 fail")
    }

    @Test
    fun testIsSubsequence() {

        val questionNumber = 392

        val expect1 = true
        val result1 = isSubsequence("abc", "ahbgdc")
        assertEquals(expect1, result1, "$questionNumber test 1 fail")

        val expect2 = false
        val result2 = isSubsequence("axc", "ahbgdc")
        assertEquals(expect2, result2, "$questionNumber test 2 fail")

    }

    @Test
    fun testClimbStairs() {
        val questionNumber = 70

        val expect1 = 2
        val result1 = climbStairs(2)
        assertEquals(expect1, result1, "$questionNumber test 1 fail")

        val expect2 = 3
        val result2 = climbStairs(3)
        assertEquals(expect2, result2, "$questionNumber test 2 fail")

        val expect3 = 5
        val result3 = climbStairs(4)
        assertEquals(expect3, result3, "$questionNumber test 3 fail")

    }

    @Test
    fun testMinDistance() {
        val questionNumber = 583
        val expect1 = 2
        val result1 = minDistance("sea", "eat")
        assertEquals(expect1, result1, "$questionNumber test 1 fail")

        val expect2 = 4
        val result2 = minDistance("leetcode", "etco")
        assertEquals(expect2, result2, "$questionNumber test 2 fail")


    }

    @Test
    fun testGetOrder() {
        val expect1 = intArrayOf(0,2,3,1)
        val result1 = getOrder(arrayOf(intArrayOf(1,2), intArrayOf(2,4), intArrayOf(3,2), intArrayOf(4,1)))
        assertContentEquals(expect1, result1, "1834 test 1 fail")

        val expect2 = intArrayOf(4,3,2,0,1)
        val result2 = getOrder(arrayOf(intArrayOf(7,10), intArrayOf(7,12), intArrayOf(7,5), intArrayOf(7,4), intArrayOf(7,2)))
        assertContentEquals(expect2, result2, "1834 test 2 fail")
    }

    @Test
    fun testSmallestNumber() {

        val expect1 = 103L
        val result1 = smallestNumber(310)
        assertEquals(expect1, result1, "2165 test 1 fail")

        val expect2 = -7650L
        val result2 = smallestNumber(-7605L)
        assertEquals(expect2, result2, "2165 test 2 fail")

        val expect3 = 0L
        val result3 = smallestNumber(0L)
        assertEquals(expect3, result3, "2165  test 3 fail")

        val expect4 = 1L
        val result4 = smallestNumber(1L)
        assertEquals(expect4, result4, "2165  test 4 fail")

        val expect5 = -1L
        val result5 = smallestNumber(-1L)
        assertEquals(expect5, result5, "2165  test 5 fail")

    }

    @Test
    fun testNumOfPairs() {
        val expect1 = 4
        val result1 = numOfPairs(arrayOf("777","7","77","77"), "7777")
        assertEquals(expect1, result1, "test 1 fail")

        val expect2 = 2
        val result2 = numOfPairs(arrayOf("123","4","12","34"), "1234")
        assertEquals(expect2, result2, "test 2 fail")

        val expect3 = 6
        val result3 = numOfPairs(arrayOf("1","1","1"), "11")
        assertEquals(expect3, result3, "test 3 fail")

    }

    @Test
    fun testMostWordsFound() {
        val expect1 = 6
        val result1 = mostWordsFound(arrayOf( "alice and bob love leetcode", "i think so too", "this is great thanks very much"))
        assertEquals(expect1, result1)

        val expect2 = 3
        val result2 = mostWordsFound(arrayOf("please wait", "continue to fight", "continue to win"))
        assertEquals(expect2, result2)

    }

}